### Hochelaga View Framework

#### Introduction
Hochelega want to give to C++ developer a powerfull ligthweight open framework for developing modern desktop and mobile application.
It is based on ogl, sld-2 and skia libraries and it mixes 2d and 3d presentation technologies.

#### Roadmap
- Prove of concept - Sept 2016
- User interface views - Dec 2016
- Navigatation view and transition effect - Jan 2017
- Video and audio views - Apr 2017
- 3d effect - Jun 2017

#### Build
TBD
