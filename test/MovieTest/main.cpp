/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "core/HoApplication.h"
#include "core/HoLog.h"
#include "core/HoByteArray.h"
#include "layout/HoVerticalLayout.h"
#include "ui/HoRectView.h"
#include "ui/HoLabelView.h"
#include "ui/HoButton.h"
#include "ui/HoImageView.h"
#include "movie/HoMovie.h"

#include <stdio.h>
#include <vector>

#include <boost/enable_shared_from_this.hpp>
#include <boost/make_shared.hpp>

int main(int argc, char * argv [] )
{
    using namespace Hochelaga;

    Movie mp4Movie( String::fromUtf8( "/Users/rleclair/Downloads/video_test.mp4" ) );
    if ( mp4Movie.open() )
    {
        Log::info( "url:%s", (const char *)mp4Movie.getUrl().toUtf8() );
        Log::info( "frameCount:%d", (int)mp4Movie.getVideoFrameCount() );
        Log::info( "duration:%f", mp4Movie.getDuration() );

        Point2d frameSize = mp4Movie.getVideoFrameSize();
        Log::info( "video frame size:%fx%f", frameSize.getX(), frameSize.getY() );

        Rational framePerSecond = mp4Movie.getVideoFramePerSecond();
        Log::info( "video frame size:%d/%d", framePerSecond.getNumerator(), framePerSecond.getDenumerator());

        String pixelFormat = mp4Movie.getPixelFormat();
        Log::info( "pixel format:%s", (const char *)pixelFormat.toUtf8() );

        String sampleFormat = mp4Movie.getVideoSampleFormat();
        Log::info( "sample format:%s", (const char *)sampleFormat.toUtf8() );

        Rational sampleApsectRatio = mp4Movie.getVideoSampleAspectRatio();
        Log::info( "sample aspect ratio:%d/%d", sampleApsectRatio.getNumerator(), sampleApsectRatio.getDenumerator());

        TimeInMicroSeconds t0 = Time::microSeconds();
        int videoFrameRead = 0;
        while( VideoFrameSPtr currVideoFrame = mp4Movie.nextVideoFrame() )
        {
            videoFrameRead++;
        }
        TimeInMicroSeconds t30 = Time::microSeconds();
        Log::info( "frame video count:%d, duration:%f", videoFrameRead, (t30-t0) * 1e-6 );
    }
}

