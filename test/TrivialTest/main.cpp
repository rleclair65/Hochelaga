#include "core/HoApplication.h"
#include "layout/HoVerticalLayout.h"
#include "ui/HoRectView.h"
#include "ui/HoLabelView.h"
#include "ui/HoButton.h"
#include "ui/HoImageView.h"
#include "movie/HoMovie.h"

#include <stdio.h>
#include <vector>

#include <boost/enable_shared_from_this.hpp>
#include <boost/make_shared.hpp>
#include <boost/bind.hpp>

int main(int argc, char * argv [] )
{
    using namespace Hochelaga;

    Application app( String::fromUtf8("Trivial Test"), argc, argv );

    ViewSPtr topView = boost::make_shared<VerticalLayout>();
    topView->setBackgroundColor( Color::kSnow );

    topView->addChildView( boost::make_shared<RectView>( Color::kRed,
                                                       View::MeasureInfo::fromPreferredWH( Dimension( 50.0f, Dimension::pxUnit ),
                                                                                           Dimension( 50.0f, Dimension::pxUnit )
                                                                                         ),
                                                       View::leftAlignment
                                                        ) );
    topView->addChildView( boost::make_shared<RectView>( Color::kGreen,
                                                       View::MeasureInfo::fromPreferredWH( Dimension( 50.0f, Dimension::pxUnit ),
                                                                                           Dimension( 50.0f, Dimension::pxUnit )
                                                                                         ),
                                                       View::centerAlignment
                                                        ) );
    topView->addChildView( boost::make_shared<RectView>( Color::kBlue,
                                                       View::MeasureInfo::fromPreferredWH( Dimension( 50.0f, Dimension::pxUnit ),
                                                                                           Dimension( 50.0f, Dimension::pxUnit )
                                                                                         ),
                                                       View::rightAlignment
                                                        ) );

    topView->addChildView( boost::make_shared<LabelView>( String::fromUtf8( "Hello, World!" ) ) );

    topView->addChildView( boost::make_shared<ImageView>( String::fromUtf8( "/Users/rleclair/dev/skia/third_party/lua/doc/osi-certified-72x60.png" ), // app.findAssetFilePath( String::fromUtf8("image.png" ) )
                                                        View::MeasureInfo::fromPreferredWH( Dimension( 300.0f, Dimension::pxUnit ),
                                                                                            Dimension()
                                                                                          ),
                                                        View::centerAlignment
                                                        ) );

    ButtonSPtr closeButton = boost::make_shared<Button>( String::fromUtf8( "Close" ),
                                                         View::MeasureInfo(),
                                                         View::centerAlignment
                                                         );
    closeButton->setTappedReceiver( boost::bind( &Application::terminate, Application::getInstance(), 0) );
    topView->addChildView(closeButton);
    app.setTopView( topView );

    return app.run();
}

