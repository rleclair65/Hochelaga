#pragma once

#include "HoRectView.h"
#include "../core/HoViewPrivate.h"

namespace Hochelaga
{

class RectViewPrivate
    : public ViewPrivate
{
public:
    RectViewPrivate( RectView * handle, const Color & foregroundColor,
                     const View::MeasureInfo & measureInfo, View::Alignment alignment );


    void paint( const PaintContext & context );


private:
    Color foregroundColor;
    SkPaint skPaint;

};

}
