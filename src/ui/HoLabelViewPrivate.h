#pragma once

#include "HoLabelView.h"
#include "../core/HoViewPrivate.h"
#include "../core/HoPaintContextPrivate.h"

namespace Hochelaga
{

class LabelViewPrivate
    : public ViewPrivate
{
public:
    LabelViewPrivate( LabelView * handle, const String & text,
                      const View::MeasureInfo & measureInfo,
                      View::Alignment alignment  );


    void setText(const String & newText);


    //
    // View class overriden
    //
public:
    void paint( const PaintContext & context );


private:
    String text;
    SkPaint textPaint;
};

}
