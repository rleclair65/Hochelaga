/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoButtonPrivate.h"
#include "../core/HoPaintContextPrivate.h"
#include <boost/make_shared.hpp>


namespace Hochelaga {

Button::Button( const String & text,
                const MeasureInfo & measureInfo,
                Alignment alignment )
    : View( boost::make_shared<ButtonPrivate>(this, text, measureInfo, alignment ) )
{
}

void Button::setText( const String & newText )
{
    dynamicBodyCast<ButtonPrivate>()->setText(newText);
}

void Button::setTappedReceiver(const TappedReceiver & TappedReceiver)
{
    dynamicBodyCast<ButtonPrivate>()->setTappedReceiver(TappedReceiver);
}

//=================================================================================================================================

ButtonPrivate::ButtonPrivate( Button * handle,
                              const String & text,
                              const View::MeasureInfo & measureInfo,
                              View::Alignment alignment )
    : ViewPrivate( handle, measureInfo, alignment )
{
    setPadding( Rect2d( 10, 10, 10, 10 ) );

    framePaint.setAntiAlias( true );
    framePaint.setColor( Color::kBlack.toARGB() );
    framePaint.setStyle( SkPaint::kStroke_Style );
    framePaint.setStrokeWidth( 2 );

    textPaint.setAntiAlias( true );
    textPaint.setColor( Color::kBlack.toARGB() );
    textPaint.setTextSize( 24.0f );
    textPaint.setStyle(SkPaint::kFill_Style);
    textPaint.setTextEncoding( SkPaint::kUTF32_TextEncoding );

    setText( text );
}

void ButtonPrivate::setText( const String & newText )
{
    text = newText;

    measureInfo.preferredWidth = Dimension( textPaint.measureText( text.c_str(), text.getByteLength(),
                                                        nullptr
                                                        ) + padding.getTop() + padding.getBottom(),
                                            Dimension::pxUnit
                                            );
    measureInfo.preferredHeight = Dimension( textPaint.getTextSize() + padding.getLeft() + padding.getRight(),
                                             Dimension::pxUnit );
}

bool ButtonPrivate::onTouchEvent( const TouchEvent & touchEvent )
{
    if ( ViewPrivate::onTouchEvent(touchEvent) )
        return true;

    if ( (touchEvent.getType() == TouchEvent::touchUnpressedEventType) && tappedReceiver)
        tappedReceiver( (Button*)getHandle() );

    return true;
}

void ButtonPrivate::paint( const PaintContext & context )
{
    paintBackground( context );

    SkScalar strokeWidth = framePaint.getStrokeWidth();
    SkScalar radius = (size.getY() - strokeWidth) / 2;
    context.getBody()->skiaCanvas->drawRoundRect( SkRect::MakeXYWH( strokeWidth / 2,
                                                                    strokeWidth / 2,
                                                                    size.getX() - strokeWidth,
                                                                    size.getY() - strokeWidth ),
                                       radius, radius,
                                       framePaint );

    SkPaint::FontMetrics fontMetrics;
    textPaint.getFontMetrics( &fontMetrics );
    context.getBody()->skiaCanvas->drawText( text.c_str(), text.getByteLength(),
                                  padding.getLeft(), padding.getTop() - fontMetrics.fAscent,
                                  textPaint );
}

}
