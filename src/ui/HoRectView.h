/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "core/HoView.h"
#include "core/HoColor.h"

#include "SkPaint.h"

namespace Hochelaga {

class RectView
    : public View
{
public:
    RectView( const Color & foregroundColor, const MeasureInfo & measureInfo, Alignment alignment );
};

}

