#pragma once

namespace Hochelaga {

class StyleManager
{
public:
    static StyleManager * getInstance();
    static void cleanup();

private:
    StyleManager();
};

}
