#pragma once

#include "HoImageView.h"
#include "../core/HoViewPrivate.h"
#include "../core/HoPaintContextPrivate.h"

namespace Hochelaga
{
class ImageViewPrivate
    : public ViewPrivate
{
public:
    ImageViewPrivate( ImageView * handle, const String & filePath, const View::MeasureInfo & measureInfo, View::Alignment alignment );


    bool readImage( const String & newFilePath );


    //
    // View class overriden
    //
public:
    void paint( const PaintContext & context );
    const Point2d calculatePreferredSize();


private:
    SkPaint imagePaint;
    sk_sp<SkImage> image;
};

}
