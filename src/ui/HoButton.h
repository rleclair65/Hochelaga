/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "core/HoView.h"
#include "boost/function.hpp"

namespace Hochelaga {

class Button
    : public View
{
public:
    typedef boost::function< void (Button * button) > TappedReceiver;


public:
    Button( const String & text, const MeasureInfo & measureInfo, Alignment alignment );

    void setText( const String & newText );
    void setTappedReceiver(const TappedReceiver & TappedReceiver);
};

typedef boost::shared_ptr<Button> ButtonSPtr;

}

