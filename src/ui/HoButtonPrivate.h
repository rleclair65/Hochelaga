#pragma once

#include "HoButton.h"
#include "../core/HoViewPrivate.h"
#include "../core/HoPaintContextPrivate.h"

namespace Hochelaga
{
    class ButtonPrivate
        : public ViewPrivate
    {
    public:
        ButtonPrivate( Button * handle, const String & text, const View::MeasureInfo & measureInfo, View::Alignment alignment );

        void setText( const String & newText );
        void setTappedReceiver(const Button::TappedReceiver & newTappedReceiver)  { tappedReceiver = newTappedReceiver; }


        //
        // ViewPrivate class overriden
        //
    public:
        void paint( const PaintContext & context );
        bool onTouchEvent( const TouchEvent & touchEvent );


    private:
        String text;
        SkPaint framePaint;
        SkPaint textPaint;
        Button::TappedReceiver tappedReceiver;
    };
}
