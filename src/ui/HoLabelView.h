/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "core/HoView.h"

namespace Hochelaga {

class LabelView
    : public View
{
public:
    LabelView( const String & text, const MeasureInfo & measureInfo = MeasureInfo(), Alignment alignment = leftAlignment  );

    void setText( const String & newText );
};

}

