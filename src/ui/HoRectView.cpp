/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoRectViewPrivate.h"
#include "../core/HoPaintContextPrivate.h"

#include <boost/make_shared.hpp>

namespace Hochelaga {

RectView::RectView( const Color & foregroundColor,
                    const MeasureInfo & measureInfo,
                    Alignment alignment
                   )
    : View( boost::make_shared<RectViewPrivate>( this, foregroundColor, measureInfo, alignment ) )
{
}

//=================================================================================================================================

RectViewPrivate::RectViewPrivate( RectView * handle,
                                  const Color & foregroundColor,
                                  const View::MeasureInfo & measureInfo,
                                  View::Alignment alignment
                                  )
    : ViewPrivate( handle, measureInfo, alignment )
{
    skPaint.setColor( foregroundColor.toARGB() );
}

void RectViewPrivate::paint( const PaintContext & context )
{
    paintBackground( context );

    context.getBody()->skiaCanvas->drawRect(SkRect::MakeWH(size.getX(), size.getY()), skPaint );

    paintChildren( context );
}

}
