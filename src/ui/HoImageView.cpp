/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoImageViewPrivate.h"
#include "core/HoByteArray.h"
#include "core/HoPaintContextPrivate.h"

#include <boost/make_shared.hpp>

namespace Hochelaga {

ImageView::ImageView( const String & filePath, const MeasureInfo & measureInfo, Alignment alignment )
    : View( boost::make_shared<ImageViewPrivate>(this, filePath, measureInfo, alignment ))
{
}

bool ImageView::readImage( const String & newFilePath )
{
    return dynamicBodyCast<ImageViewPrivate>()->readImage(newFilePath);
}


//=================================================================================================================================

ImageViewPrivate::ImageViewPrivate( ImageView * handle,
                                    const String & filePath ,
                                    const View::MeasureInfo & measureInfo,
                                    View::Alignment alignment
                                    )
    : ViewPrivate( handle, measureInfo, alignment )
{
    readImage( filePath );
}

bool ImageViewPrivate::readImage( const String & newFilePath )
{
    SkStream * stream = SkStream::NewFromFile( newFilePath.toUtf8() );
    if ( ! stream)
        return false;

    SkAutoTDelete<SkCodec> codec( SkCodec::NewFromStream( stream ) );
    if ( ! codec )
        return false;

    SkImageInfo info = codec->getInfo().makeColorType( kN32_SkColorType );

    SkBitmap bm;
    bm.allocPixels( info );
    SkAutoLockPixels autoLockPixels( bm );
    SkCodec::Result result = codec->getPixels( info, bm.getPixels(), bm.rowBytes(), NULL, NULL, NULL );
    if ( SkCodec::kSuccess != result )
        return false;

    image = SkImage::MakeFromBitmap( bm );
    if ( ! image )
        return false;

    return true;
}

void ImageViewPrivate::paint( const PaintContext & context )
{
    paintBackground( context );

    context.getBody()->skiaCanvas->drawImageRect( image,
                                                  SkRect::MakeWH( size.getX(), size.getY() ),
                                                  &imagePaint
                                                  );
}

const Point2d ImageViewPrivate::calculatePreferredSize()
{
    Scalar imageHeight = 0.0f;
    Scalar imageWidth = 0.0f;

    if ( measureInfo.preferredWidth.getUnit() == Dimension::unknownUnit )
    {
        if ( measureInfo.preferredHeight.getUnit() == Dimension::unknownUnit )
        {
            if ( image ) {
                imageWidth = image->width();
                imageHeight = image->height();
            }
        }
        else {
            imageHeight = toPixel( measureInfo.preferredHeight );
            if ( image )
                imageWidth = imageHeight / image->height() * image->width();
        }
    }
    else {
        imageWidth = toPixel( measureInfo.preferredWidth );
        if ( measureInfo.preferredHeight.getUnit() == Dimension::unknownUnit )
        {
            if ( image )
               imageHeight = imageWidth / image->width() * image->height();
            else
               imageHeight = 0;
        }
        else
            imageHeight = toPixel( measureInfo.preferredHeight );
    }

    return Point2d( imageWidth, imageHeight );
}

}
