/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoLabelViewPrivate.h"
#include "../core/HoPaintContextPrivate.h"

#include <boost/make_shared.hpp>

namespace Hochelaga {

LabelView::LabelView( const String & text, const MeasureInfo & measureInfo, Alignment alignment )
    : View( boost::make_shared<LabelViewPrivate>(this, text,measureInfo,alignment))
{
}

void LabelView::setText( const String & newText )
{
    dynamicBodyCast<LabelViewPrivate>()->setText(newText);
}


//=================================================================================================================================

LabelViewPrivate::LabelViewPrivate( LabelView * handle,
                                    const String & text,
                                    const View::MeasureInfo & measureInfo,
                                    View::Alignment alignment
                                    )
    : ViewPrivate( handle, measureInfo, alignment )
{
    textPaint.setAntiAlias( true );
    textPaint.setColor( Color::kBlack.toARGB() );
    textPaint.setTextSize( 60.0f );
    textPaint.setStyle( SkPaint::kFill_Style );
    textPaint.setTextEncoding( SkPaint::kUTF32_TextEncoding );

    setText( text );
}

void LabelViewPrivate::setText( const String & newText )
{
    text = newText;

    measureInfo.preferredWidth = Dimension( textPaint.measureText( text.c_str(), text.getByteLength(),
                                                        nullptr
                                                        ) + padding.getTop() + padding.getBottom(),
                                            Dimension::pxUnit
                                            );
    measureInfo.preferredHeight = Dimension( textPaint.getTextSize() + padding.getLeft() + padding.getRight(),
                                             Dimension::pxUnit );
}

void LabelViewPrivate::paint( const PaintContext & context )
{
    paintBackground( context );

    SkPaint::FontMetrics fontMetrics;
    textPaint.getFontMetrics( &fontMetrics );
    context.getBody()->skiaCanvas->drawText( text.c_str(), text.getByteLength(),
                                  padding.getLeft(), padding.getTop() - fontMetrics.fAscent,
                                  textPaint );
}

}
