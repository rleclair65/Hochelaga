/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "Core/HoView.h"

namespace Hochelaga {

class ImageView
    : public View
{
public:
    ImageView( const String & filePath, const MeasureInfo & measureInfo, Alignment alignment );

    bool readImage( const String & newFilePath );
};

}

