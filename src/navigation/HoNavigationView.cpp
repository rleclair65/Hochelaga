/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoNavigationView.h"

namespace Hochelaga {

NavigationView::NavigationView( const MeasureInfo & measureInfo,
                                Alignment alignment
                                )
    : View( measureInfo, alignment )
{

}

}
