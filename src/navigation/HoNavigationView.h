/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */
#pragma once

#include "../core/HoView.h"

namespace Hochelaga {

class NavigationView
    : public View
{
public:
    enum Transition
    {
        noTransition,
        fadeTransition
    };

public:
    NavigationView( const MeasureInfo & measureInfo, Alignment alignment );

    /**
     * @brief add a view in view path
     * @param view view to add
     * @param transition transition between top view and the new view
     */
    void pushView( const ViewSPtr & view, Transition transition );

    /**
     * @brief replace top view by this view with a transion
     * @param view new top view
     * @param transition transition between top and this view
     */
    void replaceView( const ViewSPtr & view, Transition transition );

    /**
     * @brief replace all views by a view
     * @param view new top view
     * @param transition transition between top view and this view
     */
    void gotoView( const ViewSPtr & view, Transition transition );

    /**
     * @brief remove top view
     */
    void back();

    /**
     * @brief hasBack
     * @return true if there are some view in view path
     */
    bool hasBack() const;

    std::vector<ViewSPtr> getViewPath() const;
};

}
