/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "../core/HoHandleBody.h"
#include "../core/HoString.h"
#include "../core/HoPoint.h"
#include "../core/HoRational.h"

#include <stdint.h>
#include <boost/shared_ptr.hpp>

namespace Hochelaga {

class MoviePrivate;
class VideoFrame;
typedef boost::shared_ptr<VideoFrame> VideoFrameSPtr;

class Movie
    : Handle<MoviePrivate>
{
public:
    typedef uint64_t FrameCount;

public:
    Movie( const String & url );

    const String & getUrl() const;

    bool open();
    bool isOpened() const;
    void close();

    double getDuration() const;

    const Point2d getVideoFrameSize() const;
    FrameCount getVideoFrameCount() const;
    const Rational getVideoFramePerSecond() const;
    const String getPixelFormat() const;
    const String getVideoSampleFormat() const;
    const Rational getVideoSampleAspectRatio() const;

    void seek( FrameCount frameNum );
    VideoFrameSPtr nextVideoFrame();
};

typedef boost::shared_ptr<Movie> MovieSPtr;

}
