/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoMovie.h"
#include "../core/HoView.h"

#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread/future.hpp>

namespace Hochelaga {

class MovieView
    : public View
{
public:
    enum State {
        unloadedState,
        pausedState,
        playingState
    };

public:
    MovieView( const MeasureInfo & measureInfo = MeasureInfo(), Alignment alignment = noAlignment );

    State getState() const  { return state; }

    boost::future<MovieSPtr> load( const String & videoUrl );
    void unload();
    boost::future<Movie::FrameCount> play();
    boost::future<Movie::FrameCount> stop();

private:
    State state;
    MovieSPtr movie;
};

}
