/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoVideoFrame.h"
#include "HoVideoFramePrivate.h"

#include <boost/make_shared.hpp>

namespace Hochelaga {

VideoFrame::VideoFrame()
    : Handle<VideoFramePrivate>( boost::make_shared<VideoFramePrivate>(this) )
{
}


//=================================================================================================================================

VideoFramePrivate::VideoFramePrivate(VideoFrame * handle)
    : Body<VideoFrame>(handle)
    , avFrame(nullptr)
{
}

VideoFramePrivate::~VideoFramePrivate()
{
    if(avFrame)
        av_frame_free(&avFrame);
}

void VideoFramePrivate::setAVFrame(AVFrame * newAvFrame)
{
    if(avFrame)
        av_frame_free(&avFrame);

    avFrame = newAvFrame;
}

}
