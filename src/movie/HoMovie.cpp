/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoMovie.h"
#include "HoMoviePrivate.h"
#include "HoVideoFrame.h"
#include "HoVideoFramePrivate.h"
#include "../core/HoByteArray.h"

#include <boost/make_shared.hpp>

namespace Hochelaga {

Movie::Movie( const String & url )
    : Handle<MoviePrivate>( boost::make_shared<MoviePrivate>(this, url) )
{
}

const String & Movie::getUrl() const
{
    return getBody()->getUrl();
}

bool Movie::open()
{
    return getBody()->open();
}

bool Movie::isOpened() const
{
    return getBody()->isOpened();
}

void Movie::close()
{
    getBody()->close();
}

Movie::FrameCount Movie::getVideoFrameCount() const
{
    return getBody()->getVideoFrameCount();
}

const Point2d Movie::getVideoFrameSize() const
{
    return getBody()->getVideoFrameSize();
}

const Rational Movie::getVideoSampleAspectRatio() const
{
    return getBody()->getVideoSampleAspectRatio();
}

const Rational Movie::getVideoFramePerSecond() const
{
    return getBody()->getVideoFramePerSecond();
}

double Movie::getDuration() const
{
    return getBody()->getDuration();
}

const String Movie::getVideoSampleFormat() const
{
    return getBody()->getVideoSampleFormat();
}

const String Movie::getPixelFormat() const
{
    return getBody()->getPixelFormat();
}

void Movie::seek( FrameCount frameNum )
{
    getBody()->seek(frameNum);
}

VideoFrameSPtr Movie::nextVideoFrame()
{
    return getBody()->nextVideoFrame();
}

//=================================================================================================================================

MoviePrivate::MoviePrivate( Movie * handle, const String & url )
    : Body<Movie>( handle )
    , url( url )
    , formatCtx( nullptr )
    , videoStreamNdx( -1 )
    , videoCodecCtx( nullptr )
    , videoCodec( nullptr )
    , audioStreamNdx( -1 )
    , audioCodecCtx( nullptr )
    , audioCodec( nullptr )
{
    static bool ffmpegInit = false;
    if ( ! ffmpegInit )
    {
        avcodec_register_all();
        av_register_all();
//        avfilter_register_all();
        ffmpegInit = true;
    }
}

MoviePrivate::~MoviePrivate()
{
    close();
}

const String & MoviePrivate::getUrl() const
{
    return url;
}

bool MoviePrivate::open()
{
    // Open file
    if (avformat_open_input(&formatCtx,
                            url.toUtf8(),
                            nullptr, // no fmt
                            nullptr // no option
                            ) < 0 )
    {
       close();
       return false;
    }

    if ( avformat_find_stream_info(formatCtx,
                                   nullptr // no option
                                   ) < 0)
    {
        close();
        return false;
    }

    /* select the video stream */
    int ret = av_find_best_stream(formatCtx,
                                  AVMEDIA_TYPE_VIDEO,
                                  -1, // wanted_stream_nb
                                  -1, // related_stream
                                  &videoCodec,
                                  0 // no flags
                                  );
    if (ret >= 0)
    {
        videoStreamNdx = ret;
        videoCodecCtx  = formatCtx->streams[videoStreamNdx]->codec;

        /* init the video decoder */
        if (avcodec_open2(videoCodecCtx,
                          videoCodec,
                          nullptr // no option
                          ) < 0) {
            close();
            return false;
        }
    }


    /* select audio stream */
    ret = av_find_best_stream(formatCtx,
                                  AVMEDIA_TYPE_AUDIO,
                                  -1, // wanted_stream_nb
                                  -1, // related_stream
                                  &audioCodec,
                                  0 // no flags
                                  );
    if (ret >= 0)
    {
        audioStreamNdx = ret;
        audioCodecCtx  = formatCtx->streams[audioStreamNdx]->codec;

        /* init the video decoder */
        if (avcodec_open2(audioCodecCtx,
                          audioCodec,
                          nullptr // no option
                          ) < 0) {
            close();
            return false;
        }
    }

    return true;
}

bool MoviePrivate::isOpened() const
{
    return (formatCtx != nullptr);
}

void MoviePrivate::close()
{
    if (videoCodecCtx)
    {
        avcodec_close(videoCodecCtx);
        videoCodecCtx = nullptr;
    }

    if (audioCodecCtx)
    {
        avcodec_close(audioCodecCtx);
        audioCodecCtx = nullptr;
    }

    if (formatCtx)
        avformat_close_input(&formatCtx);
}

Movie::FrameCount MoviePrivate::getVideoFrameCount() const
{
    if (!formatCtx)
        return -1;

    if (formatCtx->duration == AV_NOPTS_VALUE)
        return -1;

    return (Movie::FrameCount)(getDuration() * videoCodecCtx->time_base.den / videoCodecCtx->time_base.num + 0.5);
}

const Point2d MoviePrivate::getVideoFrameSize() const
{
    if (!videoCodecCtx)
        return Point2d();

    return Point2d(videoCodecCtx->width,videoCodecCtx->height);
}

double MoviePrivate::getDuration() const
{
    if (!formatCtx)
        return -1;

    if (formatCtx->duration == AV_NOPTS_VALUE)
        return -1;

    return (double)formatCtx->duration / AV_TIME_BASE;
}

const Rational MoviePrivate::getVideoFramePerSecond() const
{
    if (!videoCodecCtx)
        return Rational();

    return Rational(videoCodecCtx->time_base.num, videoCodecCtx->time_base.den);
}

const Rational MoviePrivate::getVideoSampleAspectRatio() const
{
    if (!videoCodecCtx)
        return Rational();

    return Rational(videoCodecCtx->sample_aspect_ratio.num, videoCodecCtx->sample_aspect_ratio.den);
}

const String MoviePrivate::getVideoSampleFormat() const
{
    if (!videoCodecCtx)
        return String();

    switch(videoCodecCtx->sample_fmt)
    {
    case AV_SAMPLE_FMT_U8:
        // unsigned 8 bits
        return String::fromUtf8("U8");

    case AV_SAMPLE_FMT_S16:
        // signed 16 bits
        return String::fromUtf8("S16");

    case AV_SAMPLE_FMT_S32:
        // signed 32 bits
        return String::fromUtf8("S32");

    case AV_SAMPLE_FMT_FLT:
        // float
        return String::fromUtf8("float");

    case AV_SAMPLE_FMT_DBL:
        // double
        return String::fromUtf8("double");

    case AV_SAMPLE_FMT_U8P:
        // unsigned 8 bits, planar
        return String::fromUtf8("U8-Planar");

    case AV_SAMPLE_FMT_S16P:
        // signed 16 bits, planar
        return String::fromUtf8("S16-Planar");

    case AV_SAMPLE_FMT_S32P:
        // signed 32 bits, planar
        return String::fromUtf8("S32-Planar");

    case AV_SAMPLE_FMT_FLTP:
        // float, planar
        return String::fromUtf8("float-Planar");

    case AV_SAMPLE_FMT_DBLP:
        // double, planar
        return String::fromUtf8("double-Planar");

    case AV_SAMPLE_FMT_S64:
        // signed 64 bits
        return String::fromUtf8("S64");

    case AV_SAMPLE_FMT_S64P:
        // signed 64 bits, planar
        return String::fromUtf8("S64-Planar");

    default:
        return String();

    }
}

const String MoviePrivate::getPixelFormat() const
{
    if (!videoCodecCtx)
        return String();

    switch(videoCodecCtx->pix_fmt)
    {
    case AV_PIX_FMT_YUV420P:
        // planar YUV 4:2:0, 12bpp, (1 Cr & Cb sample per 2x2 Y samples)
        return String::fromUtf8("YUV420P");

    case AV_PIX_FMT_YUYV422:
        // packed YUV 4:2:2, 16bpp, Y0 Cb Y1 Cr
        return String::fromUtf8("");

    case AV_PIX_FMT_RGB24:
        // packed RGB 8:8:8, 24bpp, RGBRGB...
        return String::fromUtf8("");

    case AV_PIX_FMT_BGR24:
        // packed RGB 8:8:8, 24bpp, BGRBGR...
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P:
        // planar YUV 4:2:2, 16bpp, (1 Cr & Cb sample per 2x1 Y samples)
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV444P:
        // planar YUV 4:4:4, 24bpp, (1 Cr & Cb sample per 1x1 Y samples)
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV410P:
        // planar YUV 4:1:0,  9bpp, (1 Cr & Cb sample per 4x4 Y samples)
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV411P:
        // planar YUV 4:1:1, 12bpp, (1 Cr & Cb sample per 4x1 Y samples)
        return String::fromUtf8("");

    case AV_PIX_FMT_GRAY8:
        //        Y        ,  8bpp
        return String::fromUtf8("");

    case AV_PIX_FMT_MONOWHITE:
        //        Y        ,  1bpp, 0 is white, 1 is black, in each byte pixels are ordered from the msb to the lsb
        return String::fromUtf8("");

    case AV_PIX_FMT_MONOBLACK:
        //        Y        ,  1bpp, 0 is black, 1 is white, in each byte pixels are ordered from the msb to the lsb
        return String::fromUtf8("");

    case AV_PIX_FMT_PAL8:
        // 8 bits with AV_PIX_FMT_RGB32 palette
        return String::fromUtf8("");

    case AV_PIX_FMT_YUVJ420P:
        // planar YUV 4:2:0, 12bpp, full scale (JPEG), deprecated in favor of AV_PIX_FMT_YUV420P and setting color_range
        return String::fromUtf8("");

    case AV_PIX_FMT_YUVJ422P:
        // planar YUV 4:2:2, 16bpp, full scale (JPEG), deprecated in favor of AV_PIX_FMT_YUV422P and setting color_range
        return String::fromUtf8("");

    case AV_PIX_FMT_YUVJ444P:
        // planar YUV 4:4:4, 24bpp, full scale (JPEG), deprecated in favor of AV_PIX_FMT_YUV444P and setting color_range
        return String::fromUtf8("");

    case AV_PIX_FMT_XVMC_MPEG2_MC:
        // XVideo Motion Acceleration via common packet passing
        return String::fromUtf8("");

    case AV_PIX_FMT_XVMC_MPEG2_IDCT:
        return String::fromUtf8("");

    case AV_PIX_FMT_UYVY422:
        // packed YUV 4:2:2, 16bpp, Cb Y0 Cr Y1
        return String::fromUtf8("");

    case AV_PIX_FMT_UYYVYY411:
        // packed YUV 4:1:1, 12bpp, Cb Y0 Y1 Cr Y2 Y3
        return String::fromUtf8("");

    case AV_PIX_FMT_BGR8:
        // packed RGB 3:3:2,  8bpp, (msb)2B 3G 3R(lsb)
        return String::fromUtf8("");

    case AV_PIX_FMT_BGR4:
        // packed RGB 1:2:1 bitstream,  4bpp, (msb)1B 2G 1R(lsb), a byte contains two pixels, the first pixel in the byte is the one composed by the 4 msb bits
        return String::fromUtf8("");

    case AV_PIX_FMT_BGR4_BYTE:
        // packed RGB 1:2:1,  8bpp, (msb)1B 2G 1R(lsb)
        return String::fromUtf8("");

    case AV_PIX_FMT_RGB8:
        // packed RGB 3:3:2,  8bpp, (msb)2R 3G 3B(lsb)
        return String::fromUtf8("");

    case AV_PIX_FMT_RGB4:
        // packed RGB 1:2:1 bitstream,  4bpp, (msb)1R 2G 1B(lsb), a byte contains two pixels, the first pixel in the byte is the one composed by the 4 msb bits
        return String::fromUtf8("");

    case AV_PIX_FMT_RGB4_BYTE:
        // packed RGB 1:2:1,  8bpp, (msb)1R 2G 1B(lsb)
        return String::fromUtf8("");

    case AV_PIX_FMT_NV12:
        // planar YUV 4:2:0, 12bpp, 1 plane for Y and 1 plane for the UV components, which are interleaved (first byte U and the following byte V)
        return String::fromUtf8("");

    case AV_PIX_FMT_NV21:
        // as above, but U and V bytes are swapped
        return String::fromUtf8("");

    case AV_PIX_FMT_ARGB:
        // packed ARGB 8:8:8:8, 32bpp, ARGBARGB...
        return String::fromUtf8("");

    case AV_PIX_FMT_RGBA:
        // packed RGBA 8:8:8:8, 32bpp, RGBARGBA...
        return String::fromUtf8("RGBA");

    case AV_PIX_FMT_ABGR:
        // packed ABGR 8:8:8:8, 32bpp, ABGRABGR...
        return String::fromUtf8("ABGR");

    case AV_PIX_FMT_BGRA:
        // packed BGRA 8:8:8:8, 32bpp, BGRABGRA...
        return String::fromUtf8("BGRA");

    case AV_PIX_FMT_GRAY16BE:
        //        Y        , 16bpp, big-endian
        return String::fromUtf8("Gray16BE");

    case AV_PIX_FMT_GRAY16LE:
        //        Y        , 16bpp, little-endian
        return String::fromUtf8("Gray16LE");

    case AV_PIX_FMT_YUV440P:
        // planar YUV 4:4:0 (1 Cr & Cb sample per 1x2 Y samples)
        return String::fromUtf8("YUV440P");

    case AV_PIX_FMT_YUVJ440P:
        // planar YUV 4:4:0 full scale (JPEG), deprecated in favor of AV_PIX_FMT_YUV440P and setting color_range
        return String::fromUtf8("YUVJ440P");

    case AV_PIX_FMT_YUVA420P:
        // planar YUV 4:2:0, 20bpp, (1 Cr & Cb sample per 2x2 Y & A samples)
        return String::fromUtf8("YUVA420P");

    case AV_PIX_FMT_VDPAU_H264:
        // H.264 HW decoding with VDPAU, data[0] contains a vdpau_render_state struct which contains the bitstream of the slices as well as various fields extracted from headers
        return String::fromUtf8("VDPAU_H264");

    case AV_PIX_FMT_VDPAU_MPEG1:
        // MPEG-1 HW decoding with VDPAU, data[0] contains a vdpau_render_state struct which contains the bitstream of the slices as well as various fields extracted from headers
        return String::fromUtf8("VDPAU_MPEG1");

    case AV_PIX_FMT_VDPAU_MPEG2:
        // MPEG-2 HW decoding with VDPAU, data[0] contains a vdpau_render_state struct which contains the bitstream of the slices as well as various fields extracted from headers
        return String::fromUtf8("VDPAU_MPEG2");

    case AV_PIX_FMT_VDPAU_WMV3:
        // WMV3 HW decoding with VDPAU, data[0] contains a vdpau_render_state struct which contains the bitstream of the slices as well as various fields extracted from headers
        return String::fromUtf8("VDPAU_WMV3");

    case AV_PIX_FMT_VDPAU_VC1:
        // VC-1 HW decoding with VDPAU, data[0] contains a vdpau_render_state struct which contains the bitstream of the slices as well as various fields extracted from headers
        return String::fromUtf8("VC1");

    case AV_PIX_FMT_RGB48BE:
        // packed RGB 16:16:16, 48bpp, 16R, 16G, 16B, the 2-byte value for each R/G/B component is stored as big-endian
        return String::fromUtf8("RGB48BE");

    case AV_PIX_FMT_RGB48LE:
        // packed RGB 16:16:16, 48bpp, 16R, 16G, 16B, the 2-byte value for each R/G/B component is stored as little-endian
        return String::fromUtf8("RGB48LE");

    case AV_PIX_FMT_RGB565BE:
        // packed RGB 5:6:5, 16bpp, (msb)   5R 6G 5B(lsb), big-endian
        return String::fromUtf8("RGB565BE");

    case AV_PIX_FMT_RGB565LE:
        // packed RGB 5:6:5, 16bpp, (msb)   5R 6G 5B(lsb), little-endian
        return String::fromUtf8("RGB565LE");

    case AV_PIX_FMT_RGB555BE:
        // packed RGB 5:5:5, 16bpp, (msb)1X 5R 5G 5B(lsb), big-endian   , X=unused/undefined
        return String::fromUtf8("RGB555BE");

    case AV_PIX_FMT_RGB555LE:
        // packed RGB 5:5:5, 16bpp, (msb)1X 5R 5G 5B(lsb), little-endian, X=unused/undefined
        return String::fromUtf8("RGB555LE");

    case AV_PIX_FMT_BGR565BE:
        // packed BGR 5:6:5, 16bpp, (msb)   5B 6G 5R(lsb), big-endian
        return String::fromUtf8("BGR565BE");

    case AV_PIX_FMT_BGR565LE:
        // packed BGR 5:6:5, 16bpp, (msb)   5B 6G 5R(lsb), little-endian
        return String::fromUtf8("BGR565LE");

    case AV_PIX_FMT_BGR555BE:
        // packed BGR 5:5:5, 16bpp, (msb)1X 5B 5G 5R(lsb), big-endian   , X=unused/undefined
        return String::fromUtf8("BGR555BE");

    case AV_PIX_FMT_BGR555LE:
        // packed BGR 5:5:5, 16bpp, (msb)1X 5B 5G 5R(lsb), little-endian, X=unused/undefined
        return String::fromUtf8("BGR555LE");

    case AV_PIX_FMT_YUV420P16LE:
        // planar YUV 4:2:0, 24bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian
        return String::fromUtf8("YUV420P16LE");

    case AV_PIX_FMT_YUV420P16BE:  // planar YUV 4:2:0, 24bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P16LE:  // planar YUV 4:2:2, 32bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P16BE:  // planar YUV 4:2:2, 32bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV444P16LE:  // planar YUV 4:4:4, 48bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV444P16BE:  // planar YUV 4:4:4, 48bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_VDPAU_MPEG4:  // MPEG-4 HW decoding with VDPAU, data[0] contains a vdpau_render_state struct which contains the bitstream of the slices as well as various fields extracted from headers
        return String::fromUtf8("");

    case AV_PIX_FMT_DXVA2_VLD:    // HW decoding through DXVA2, Picture.data[3] contains a LPDIRECT3DSURFACE9 pointer
        return String::fromUtf8("");

    case AV_PIX_FMT_RGB444LE:  // packed RGB 4:4:4, 16bpp, (msb)4X 4R 4G 4B(lsb), little-endian, X=unused/undefined
        return String::fromUtf8("");

    case AV_PIX_FMT_RGB444BE:  // packed RGB 4:4:4, 16bpp, (msb)4X 4R 4G 4B(lsb), big-endian,    X=unused/undefined
        return String::fromUtf8("");

    case AV_PIX_FMT_BGR444LE:  // packed BGR 4:4:4, 16bpp, (msb)4X 4B 4G 4R(lsb), little-endian, X=unused/undefined
        return String::fromUtf8("");

    case AV_PIX_FMT_BGR444BE:  // packed BGR 4:4:4, 16bpp, (msb)4X 4B 4G 4R(lsb), big-endian,    X=unused/undefined
        return String::fromUtf8("");

    case AV_PIX_FMT_YA8:       // 8 bits gray, 8 bits alpha
        return String::fromUtf8("");

    case AV_PIX_FMT_BGR48BE:   // packed RGB 16:16:16, 48bpp, 16B, 16G, 16R, the 2-byte value for each R/G/B component is stored as big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_BGR48LE:   // packed RGB 16:16:16, 48bpp, 16B, 16G, 16R, the 2-byte value for each R/G/B component is stored as little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV420P9BE: // planar YUV 4:2:0, 13.5bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV420P9LE: // planar YUV 4:2:0, 13.5bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV420P10BE:// planar YUV 4:2:0, 15bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV420P10LE:// planar YUV 4:2:0, 15bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P10BE:// planar YUV 4:2:2, 20bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P10LE:
        // planar YUV 4:2:2, 20bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
        return String::fromUtf8("YUV422P10LE");

    case AV_PIX_FMT_YUV444P9BE:
        // planar YUV 4:4:4, 27bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian
        return String::fromUtf8("YUV444P9BE");

    case AV_PIX_FMT_YUV444P9LE:
        // planar YUV 4:4:4, 27bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian
        return String::fromUtf8("YUV444P9LE");

    case AV_PIX_FMT_YUV444P10BE:
        // planar YUV 4:4:4, 30bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian
        return String::fromUtf8("YUV444P10BE");

    case AV_PIX_FMT_YUV444P10LE:
        // planar YUV 4:4:4, 30bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian
        return String::fromUtf8("YUV444OP10LE");

    case AV_PIX_FMT_YUV422P9BE:
        // planar YUV 4:2:2, 18bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
        return String::fromUtf8("YUV422P8BE");

    case AV_PIX_FMT_YUV422P9LE:
        // planar YUV 4:2:2, 18bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
        return String::fromUtf8("YUV422P9LE");

    case AV_PIX_FMT_VDA_VLD:
        // hardware decoding through VDA
        return String::fromUtf8("VDA_VLD");

    case AV_PIX_FMT_GBRP:
        // planar GBR 4:4:4 24bpp
        return String::fromUtf8("GBRP");

    case AV_PIX_FMT_GBRP9BE:
        // planar GBR 4:4:4 27bpp, big-endian
        return String::fromUtf8("GBRP9BE");

    case AV_PIX_FMT_GBRP9LE:
        // planar GBR 4:4:4 27bpp, little-endian
        return String::fromUtf8("GBRP9LE");

    case AV_PIX_FMT_GBRP10BE:
        // planar GBR 4:4:4 30bpp, big-endian
        return String::fromUtf8("GGRP10BE");

    case AV_PIX_FMT_GBRP10LE:
        // planar GBR 4:4:4 30bpp, little-endian
        return String::fromUtf8("GBRP10LE");

    case AV_PIX_FMT_GBRP16BE:
        // planar GBR 4:4:4 48bpp, big-endian
        return String::fromUtf8("GBRP16BE");

    case AV_PIX_FMT_GBRP16LE:
        // planar GBR 4:4:4 48bpp, little-endian
        return String::fromUtf8("GBRP16LE");

    case AV_PIX_FMT_YUVA422P:
        // planar YUV 4:2:2 24bpp, (1 Cr & Cb sample per 2x1 Y & A samples)
        return String::fromUtf8("YUVA422P");

    case AV_PIX_FMT_YUVA444P:
        // planar YUV 4:4:4 32bpp, (1 Cr & Cb sample per 1x1 Y & A samples)
        return String::fromUtf8("YUVA442P");

    case AV_PIX_FMT_YUVA420P9BE:
        // planar YUV 4:2:0 22.5bpp, (1 Cr & Cb sample per 2x2 Y & A samples), big-endian
        return String::fromUtf8("YUVA420P9BE");

    case AV_PIX_FMT_YUVA420P9LE:
        // planar YUV 4:2:0 22.5bpp, (1 Cr & Cb sample per 2x2 Y & A samples), little-endian
        return String::fromUtf8("YUVA420P8LE");

    case AV_PIX_FMT_YUVA422P9BE:
        // planar YUV 4:2:2 27bpp, (1 Cr & Cb sample per 2x1 Y & A samples), big-endian
        return String::fromUtf8("YUVA422P9BE");

    case AV_PIX_FMT_YUVA422P9LE:
        // planar YUV 4:2:2 27bpp, (1 Cr & Cb sample per 2x1 Y & A samples), little-endian
        return String::fromUtf8("YUVA422P9LE");

    case AV_PIX_FMT_YUVA444P9BE:
        // planar YUV 4:4:4 36bpp, (1 Cr & Cb sample per 1x1 Y & A samples), big-endian
        return String::fromUtf8("YUVA422P9BE");

    case AV_PIX_FMT_YUVA444P9LE:
        // planar YUV 4:4:4 36bpp, (1 Cr & Cb sample per 1x1 Y & A samples), little-endian
        return String::fromUtf8("YUVA444P9LE");

    case AV_PIX_FMT_YUVA420P10BE:
        // planar YUV 4:2:0 25bpp, (1 Cr & Cb sample per 2x2 Y & A samples, big-endian)
        return String::fromUtf8("YUVA420P10BE");

    case AV_PIX_FMT_YUVA420P10LE:
        // planar YUV 4:2:0 25bpp, (1 Cr & Cb sample per 2x2 Y & A samples, little-endian)
        return String::fromUtf8("YUVA420P10LE");

    case AV_PIX_FMT_YUVA422P10BE:
        // planar YUV 4:2:2 30bpp, (1 Cr & Cb sample per 2x1 Y & A samples, big-endian)
        return String::fromUtf8("YUVA422P10BE");

    case AV_PIX_FMT_YUVA422P10LE:
        // planar YUV 4:2:2 30bpp, (1 Cr & Cb sample per 2x1 Y & A samples, little-endian)
        return String::fromUtf8("YUVA422P10LE");

    case AV_PIX_FMT_YUVA444P10BE:
        // planar YUV 4:4:4 40bpp, (1 Cr & Cb sample per 1x1 Y & A samples, big-endian)
        return String::fromUtf8("YUVA444P10BE");

    case AV_PIX_FMT_YUVA444P10LE:
        // planar YUV 4:4:4 40bpp, (1 Cr & Cb sample per 1x1 Y & A samples, little-endian)
        return String::fromUtf8("YUVA444P10LE");

    case AV_PIX_FMT_YUVA420P16BE:
        // planar YUV 4:2:0 40bpp, (1 Cr & Cb sample per 2x2 Y & A samples, big-endian)
        return String::fromUtf8("YUVA420P16BE");

    case AV_PIX_FMT_YUVA420P16LE:
        // planar YUV 4:2:0 40bpp, (1 Cr & Cb sample per 2x2 Y & A samples, little-endian)
        return String::fromUtf8("YUVA420P16LE");

    case AV_PIX_FMT_YUVA422P16BE:
        // planar YUV 4:2:2 48bpp, (1 Cr & Cb sample per 2x1 Y & A samples, big-endian)
        return String::fromUtf8("YUVA422P16BE");

    case AV_PIX_FMT_YUVA422P16LE:
        // planar YUV 4:2:2 48bpp, (1 Cr & Cb sample per 2x1 Y & A samples, little-endian)
        return String::fromUtf8("YUVA422P16LE");

    case AV_PIX_FMT_YUVA444P16BE:
        // planar YUV 4:4:4 64bpp, (1 Cr & Cb sample per 1x1 Y & A samples, big-endian)
        return String::fromUtf8("YUVA444P16BE");

    case AV_PIX_FMT_YUVA444P16LE:
        // planar YUV 4:4:4 64bpp, (1 Cr & Cb sample per 1x1 Y & A samples, little-endian)
        return String::fromUtf8("YUVA444P16LE");

    case AV_PIX_FMT_VDPAU:
        // HW acceleration through VDPAU, Picture.data[3] contains a VdpVideoSurface
        return String::fromUtf8("VDPAU");

    case AV_PIX_FMT_XYZ12LE:
        // packed XYZ 4:4:4, 36 bpp, (msb) 12X, 12Y, 12Z (lsb), the 2-byte value for each X/Y/Z is stored as little-endian, the 4 lower bits are set to 0
        return String::fromUtf8("XYZ12LE");

    case AV_PIX_FMT_XYZ12BE:
        // packed XYZ 4:4:4, 36 bpp, (msb) 12X, 12Y, 12Z (lsb), the 2-byte value for each X/Y/Z is stored as big-endian, the 4 lower bits are set to 0
        return String::fromUtf8("XYZ12BE");

    case AV_PIX_FMT_NV16:
        // interleaved chroma YUV 4:2:2, 16bpp, (1 Cr & Cb sample per 2x1 Y samples)
        return String::fromUtf8("NV16");

    case AV_PIX_FMT_NV20LE:
        // interleaved chroma YUV 4:2:2, 20bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
        return String::fromUtf8("NV20LE");

    case AV_PIX_FMT_NV20BE:
        // interleaved chroma YUV 4:2:2, 20bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
        return String::fromUtf8("NV20BE");

    case AV_PIX_FMT_RGBA64BE:
        // packed RGBA 16:16:16:16, 64bpp, 16R, 16G, 16B, 16A, the 2-byte value for each R/G/B/A component is stored as big-endian
        return String::fromUtf8("RGBA64BE");

    case AV_PIX_FMT_RGBA64LE:
        // packed RGBA 16:16:16:16, 64bpp, 16R, 16G, 16B, 16A, the 2-byte value for each R/G/B/A component is stored as little-endian
        return String::fromUtf8("RGBA64BE");

    case AV_PIX_FMT_BGRA64BE:
        // packed RGBA 16:16:16:16, 64bpp, 16B, 16G, 16R, 16A, the 2-byte value for each R/G/B/A component is stored as big-endian
        return String::fromUtf8("BGRA64BE");

    case AV_PIX_FMT_BGRA64LE:
        // packed RGBA 16:16:16:16, 64bpp, 16B, 16G, 16R, 16A, the 2-byte value for each R/G/B/A component is stored as little-endian
        return String::fromUtf8("BGRA64LE");

    case AV_PIX_FMT_YVYU422:
        // packed YUV 4:2:2, 16bpp, Y0 Cr Y1 Cb
        return String::fromUtf8("YVYU422");

    case AV_PIX_FMT_VDA:
        // HW acceleration through VDA, data[3] contains a CVPixelBufferRef
        return String::fromUtf8("VDA");

    case AV_PIX_FMT_YA16BE:
        // 16 bits gray, 16 bits alpha (big-endian)
        return String::fromUtf8("YA16BE");

    case AV_PIX_FMT_YA16LE:
        // 16 bits gray, 16 bits alpha (little-endian)
        return String::fromUtf8("YA16LE");

    case AV_PIX_FMT_GBRAP:
        // planar GBRA 4:4:4:4 32bpp
        return String::fromUtf8("GBRAP");

    case AV_PIX_FMT_GBRAP16BE:
        // planar GBRA 4:4:4:4 64bpp, big-endian
        return String::fromUtf8("GBRAP16BE");

    case AV_PIX_FMT_GBRAP16LE:
        // planar GBRA 4:4:4:4 64bpp, little-endian
        return String::fromUtf8("GBRAP16LE");

    case AV_PIX_FMT_QSV:
        return String::fromUtf8("QSV");

    case AV_PIX_FMT_MMAL:
        return String::fromUtf8("MMAL");

    case AV_PIX_FMT_D3D11VA_VLD:
        // HW decoding through Direct3D11, Picture.data[3] contains a ID3D11VideoDecoderOutputView pointer
        return String::fromUtf8("D3D11VA_VLD");

    case AV_PIX_FMT_CUDA:
        return String::fromUtf8("CUDA");

    case AV_PIX_FMT_0RGB:
        // packed RGB 8:8:8, 32bpp, XRGBXRGB...   X=unused/undefined
        return String::fromUtf8("0RGB");

    case AV_PIX_FMT_RGB0:
        // packed RGB 8:8:8, 32bpp, RGBXRGBX...   X=unused/undefined
        return String::fromUtf8("RGB0");

    case AV_PIX_FMT_0BGR:
        // packed BGR 8:8:8, 32bpp, XBGRXBGR...   X=unused/undefined
        return String::fromUtf8("0BGR");

    case AV_PIX_FMT_BGR0:
        // packed BGR 8:8:8, 32bpp, BGRXBGRX...   X=unused/undefined
        return String::fromUtf8("BGR0");

    case AV_PIX_FMT_YUV420P12BE:
        // planar YUV 4:2:0,18bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV420P12LE: // planar YUV 4:2:0,18bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV420P14BE: // planar YUV 4:2:0,21bpp, (1 Cr & Cb sample per 2x2 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV420P14LE: // planar YUV 4:2:0,21bpp, (1 Cr & Cb sample per 2x2 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P12BE: // planar YUV 4:2:2,24bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P12LE: // planar YUV 4:2:2,24bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P14BE: // planar YUV 4:2:2,28bpp, (1 Cr & Cb sample per 2x1 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV422P14LE: // planar YUV 4:2:2,28bpp, (1 Cr & Cb sample per 2x1 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV444P12BE: // planar YUV 4:4:4,36bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV444P12LE: // planar YUV 4:4:4,36bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV444P14BE: // planar YUV 4:4:4,42bpp, (1 Cr & Cb sample per 1x1 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV444P14LE: // planar YUV 4:4:4,42bpp, (1 Cr & Cb sample per 1x1 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_GBRP12BE:    // planar GBR 4:4:4 36bpp, big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_GBRP12LE:    // planar GBR 4:4:4 36bpp, little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_GBRP14BE:
        // planar GBR 4:4:4 42bpp, big-endian
        return String::fromUtf8("GBRP14BE");

    case AV_PIX_FMT_GBRP14LE:
        // planar GBR 4:4:4 42bpp, little-endian
        return String::fromUtf8("GBRP14LE");

    case AV_PIX_FMT_YUVJ411P:
        // planar YUV 4:1:1, 12bpp, (1 Cr & Cb sample per 4x1 Y samples) full scale (JPEG), deprecated in favor of AV_PIX_FMT_YUV411P and setting color_range
        return String::fromUtf8("YUVJ411P");

    case AV_PIX_FMT_BAYER_BGGR8:
        // bayer, BGBG..(odd line), GRGR..(even line), 8-bit samples */
        return String::fromUtf8("BGGR8");

    case AV_PIX_FMT_BAYER_RGGB8:
        // bayer, RGRG..(odd line), GBGB..(even line), 8-bit samples */
        return String::fromUtf8("RGGB8");

    case AV_PIX_FMT_BAYER_GBRG8:
        // bayer, GBGB..(odd line), RGRG..(even line), 8-bit samples */
        return String::fromUtf8("BAYER_GBRG8");

    case AV_PIX_FMT_BAYER_GRBG8:
        // bayer, GRGR..(odd line), BGBG..(even line), 8-bit samples */
        return String::fromUtf8("BAYER_GRBG8");

    case AV_PIX_FMT_BAYER_BGGR16LE:
        // bayer, BGBG..(odd line), GRGR..(even line), 16-bit samples, little-endian */
        return String::fromUtf8("BAYER_BGGR16LE");

    case AV_PIX_FMT_BAYER_BGGR16BE:
        // bayer, BGBG..(odd line), GRGR..(even line), 16-bit samples, big-endian */
        return String::fromUtf8("");

    case AV_PIX_FMT_BAYER_RGGB16LE:
        // bayer, RGRG..(odd line), GBGB..(even line), 16-bit samples, little-endian */
        return String::fromUtf8("");

    case AV_PIX_FMT_BAYER_RGGB16BE:
        // bayer, RGRG..(odd line), GBGB..(even line), 16-bit samples, big-endian */
        return String::fromUtf8("");

    case AV_PIX_FMT_BAYER_GBRG16LE:
        // bayer, GBGB..(odd line), RGRG..(even line), 16-bit samples, little-endian */
        return String::fromUtf8("");

    case AV_PIX_FMT_BAYER_GBRG16BE:
        // bayer, GBGB..(odd line), RGRG..(even line), 16-bit samples, big-endian */
        return String::fromUtf8("");

    case AV_PIX_FMT_BAYER_GRBG16LE:
        // bayer, GRGR..(odd line), BGBG..(even line), 16-bit samples, little-endian */
        return String::fromUtf8("");

    case AV_PIX_FMT_BAYER_GRBG16BE:
        // bayer, GRGR..(odd line), BGBG..(even line), 16-bit samples, big-endian */
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV440P10LE:
        // planar YUV 4:4:0,20bpp, (1 Cr & Cb sample per 1x2 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV440P10BE:
        // planar YUV 4:4:0,20bpp, (1 Cr & Cb sample per 1x2 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV440P12LE:
        // planar YUV 4:4:0,24bpp, (1 Cr & Cb sample per 1x2 Y samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_YUV440P12BE:
        // planar YUV 4:4:0,24bpp, (1 Cr & Cb sample per 1x2 Y samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_AYUV64LE:
        // packed AYUV 4:4:4,64bpp (1 Cr & Cb sample per 1x1 Y & A samples), little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_AYUV64BE:
        // packed AYUV 4:4:4,64bpp (1 Cr & Cb sample per 1x1 Y & A samples), big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_VIDEOTOOLBOX:
        // hardware decoding through Videotoolbox
        return String::fromUtf8("");

    case AV_PIX_FMT_P010LE:
        // like NV12, with 10bpp per component, data in the high bits, zeros in the low bits, little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_P010BE:
        // like NV12, with 10bpp per component, data in the high bits, zeros in the low bits, big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_GBRAP12BE:
        // planar GBR 4:4:4:4 48bpp, big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_GBRAP12LE:
        // planar GBR 4:4:4:4 48bpp, little-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_GBRAP10BE:
        // planar GBR 4:4:4:4 40bpp, big-endian
        return String::fromUtf8("");

    case AV_PIX_FMT_GBRAP10LE:
        // planar GBR 4:4:4:4 40bpp, little-endian
        return String::fromUtf8("Gbrap10LE");

    case AV_PIX_FMT_MEDIACODEC:
        // hardware decoding through MediaCodec
        return String::fromUtf8("MediaCodec");

    default:
        return String::fromUtf8("");
    }
}

void MoviePrivate::seek( Movie::FrameCount frameNum )
{
}

VideoFrameSPtr MoviePrivate::nextVideoFrame()
{
    if (! videoCodecCtx)
        return VideoFrameSPtr();

    int ret = 0;
    AVPacket packet;
    AVFrame * videoFrame = av_frame_alloc();
    while ( (ret = av_read_frame(formatCtx, &packet)) >= 0)
    {
        if (packet.stream_index == videoStreamNdx)
        {
            int gotPicture = 0;
            int videoFrameBytes = avcodec_decode_video2( videoCodecCtx, videoFrame, &gotPicture, &packet);
            if(gotPicture)
            {
                VideoFrameSPtr result = boost::make_shared<VideoFrame>();
                result->getBody()->setAVFrame(videoFrame);
                return result;
            }
        }
    }

    if(videoFrame)
        av_frame_free(&videoFrame);

    return VideoFrameSPtr();
}

} // namespace Hochelaga
