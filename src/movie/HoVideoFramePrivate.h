/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */
#pragma once

#include "HoMoviePrivate.h"
#include "../core/HoHandleBody.h"

namespace Hochelaga {

class VideoFrame;

class VideoFramePrivate
    : public Body<VideoFrame>
{
public:
    VideoFramePrivate( VideoFrame * handle );
    ~VideoFramePrivate();

    void setAVFrame(AVFrame * newAvFrame);


private:
    AVFrame * avFrame;
};

}
