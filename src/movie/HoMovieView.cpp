#include "HoMovieView.h"

#include <boost/make_shared.hpp>


namespace Hochelaga {

MovieView::MovieView( const MeasureInfo & measureInfo,
                      Alignment alignment
                      )
    : View( measureInfo, alignment )
{
}

boost::future<MovieSPtr> MovieView::load( const String & videoUrl )
{
    movie = boost::make_shared<Movie>( videoUrl );

    return boost::make_ready_future<MovieSPtr>( movie );
}

void MovieView::unload()
{
}

boost::future<Movie::FrameCount> MovieView::play()
{
    return boost::make_ready_future<Movie::FrameCount>( 0 );
}

boost::future<Movie::FrameCount> MovieView::stop()
{
    return boost::make_ready_future<Movie::FrameCount>( 0 );
}

}
