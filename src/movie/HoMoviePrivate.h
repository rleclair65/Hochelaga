/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "../core/HoHandleBody.h"
#include "HoMovie.h"

extern "C"
{
    #include <libavformat/avformat.h>
    #include <libavcodec/avcodec.h>
    #include <libavfilter/avfiltergraph.h>
    #include <libavfilter/buffersink.h>
}

namespace Hochelaga {

class Movie;

class MoviePrivate
    : Body<Movie>
{
public:
    MoviePrivate( Movie * handle, const String & url );
    ~MoviePrivate();

    const String & getUrl() const;

    bool open();
    bool isOpened() const;
    void close();

    double getDuration() const;
    Movie::FrameCount getVideoFrameCount() const;
    const Point2d getVideoFrameSize() const;
    const Rational getVideoFramePerSecond() const;
    const Rational getVideoSampleAspectRatio() const;
    const String getVideoSampleFormat() const;
    const String getPixelFormat() const;

    void seek( Movie::FrameCount frameNum );
    VideoFrameSPtr nextVideoFrame();

private:
    String url;
    AVFormatContext * formatCtx;
    int videoStreamNdx;
    AVCodecContext * videoCodecCtx;
    AVCodec * videoCodec;
    int audioStreamNdx;
    AVCodecContext * audioCodecCtx;
    AVCodec * audioCodec;
};

}
