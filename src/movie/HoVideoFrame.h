/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "../core/HoHandleBody.h"

#include <boost/shared_ptr.hpp>

namespace Hochelaga {

class VideoFrame;
typedef boost::shared_ptr<VideoFrame> VideoFrameSPtr;

class VideoFramePrivate;

class VideoFrame
    : public Handle<VideoFramePrivate>
{
public:
    VideoFrame();
};

}

