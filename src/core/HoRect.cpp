/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoRect.h"

namespace Hochelaga {

Rect2d::Rect2d(const Point2d &pt1, const Point2d &pt2 )
    : left( 0 )
    , top( 0 )
    , right( 0 )
    , bottom( 0 )
{
}

Rect2d::Rect2d( const Point2d & topLeft, Scalar width, Scalar height )
    : left( topLeft.getX() )
    , top( topLeft.getY() )
    , right( topLeft.getX() + width )
    , bottom( topLeft.getY() + height )
{
}

const Point2d Rect2d::getLeftTop() const
{
    return Point2d( left, top );
}

}
