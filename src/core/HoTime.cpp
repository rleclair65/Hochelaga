#include "HoTime.h"
#include <sys/time.h>

namespace Hochelaga {

Time::Time()
{

}

uint64_t Time::microSeconds()
{
    struct timeval tv;
    if ( gettimeofday( &tv, nullptr ) < 0 )
        return 0;

    return tv.tv_sec * (uint64_t)1000000 + tv.tv_usec;
}

}
