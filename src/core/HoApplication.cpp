/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoApplication.h"
#include "HoApplicationPrivate.h"
#include "HoPaintContextPrivate.h"
#include "HoView.h"
#include "HoByteArray.h"
#include "HoTime.h"

#include "GrContext.h"
#include "SkSurface.h"
#include "SkCanvas.h"

#include <boost/make_shared.hpp>

namespace Hochelaga {

Application * Application::instance = nullptr;

Application::Application( const String & name, int argc, char * argv[] )
    : Handle<ApplicationPrivate>( boost::make_shared<ApplicationPrivate>(this, name, argc, argv) )
{
    instance = this;
}

Application::~Application()
{
    instance = nullptr;
}

const ViewSPtr & Application::getTopView() const
{
    return getBody()->getTopView();
}

void Application::setTopView(const ViewSPtr & newTopView)
{
    return getBody()->setTopView( newTopView );
}

Application::State Application::getState() const
{
    return getBody()->getState();
}

const Point2d & Application::getScreenSize() const
{
    return getBody()->getScreenSize();
}

Scalar Application::getScreenDpi() const
{
    return getBody()->getScreenDpi();
}

const String Application::findAssetFilePath( const String & fileName ) const
{
    return getBody()->findAssetFilePath(fileName);
}

int Application::run()
{
    return getBody()->run();
}

void Application::terminate( int resultCode )
{
    getBody()->terminate(resultCode);
}

void Application::paint()
{
    getBody()->paint();
}

void Application::pushEvent(const EventSPtr & event)
{
    getBody()->pushEvent(event);
}

void Application::processAllEvents()
{
    getBody()->processAllEvents();
}


//=================================================================================================================================

ApplicationPrivate::ApplicationPrivate( Application * handle,
                                        const String & name,
                                        int argc, char * argv[]
                                        )
    : Body<Application>( handle )
    , name( name )
    , state( Application::initializingState )
    , screenSize( 400.0f, 600.0f )
    , screenDpi( 96.0f )
    , resultCode( 0 )
    , viewAnimated( false )
{
    for ( size_t i = 0; i < argc; i++ )
        arguementList.push_back( String::fromUtf8( argv[i] ) );
}

void ApplicationPrivate::onEvent( const EventSPtr & event )
{
    if ( topView )
        topView->onEvent( event );
}

int ApplicationPrivate::run()
{
    if ( ! initSdl2() )
        return -1;

    while ( state != Application::terminatedState )
    {
        readSdl2Event( 15 );
        processAllEvents();
        paint();
    }

    closeSdl2();

    return resultCode;
}

void ApplicationPrivate::readSdl2Event( int timeout )
{
    bool done = false;
    while( ! done )
    {
        SDL_Event event;
        int result = SDL_WaitEventTimeout( &event, timeout );

        if ( result ) {

            switch(event.type) {

            case SDL_KEYDOWN:
                convertSdlKeyDownEvent( event.key );
                break;

            case SDL_TEXTINPUT:
                convertSdlTexInputEvent( event.text );
                break;

            case SDL_FINGERDOWN:
                break;

            case SDL_FINGERUP:
                break;

            case SDL_FINGERMOTION:
                break;

            case SDL_MOUSEBUTTONDOWN:
                convertSdlMouseButtonDown(event.button);
                break;

            case SDL_MOUSEBUTTONUP:
                convertSdlMouseButtonUp(event.button);
                break;

            case SDL_MOUSEMOTION:
                convertSdlMouseMotion(event.motion);
                break;

            case SDL_QUIT:
                SDL_LogVerbose( SDL_LOG_CATEGORY_APPLICATION, "SDL_QUIT event\n");
                state = Application::terminatedState;
                break;

            case SDL_APP_LOWMEMORY:
                SDL_LogVerbose( SDL_LOG_CATEGORY_APPLICATION, "SDL_APP_LOWMEMORY event\n");
                break;

            case SDL_APP_WILLENTERBACKGROUND:
                SDL_LogVerbose( SDL_LOG_CATEGORY_APPLICATION, "SDL_APP_WILLENTERBACKGROUND event\n");
                break;

            case SDL_APP_DIDENTERBACKGROUND:
                SDL_LogVerbose( SDL_LOG_CATEGORY_APPLICATION, "SDL_APP_DIDENTERBACKGROUND event\n");
                break;

            case SDL_APP_WILLENTERFOREGROUND:
                SDL_LogVerbose( SDL_LOG_CATEGORY_APPLICATION, "SDL_APP_WILLENTERFOREGROUND event\n");
                break;

            case SDL_APP_DIDENTERFOREGROUND:
                SDL_LogVerbose( SDL_LOG_CATEGORY_APPLICATION, "SDL_APP_DIDENTERFOREGROUND event\n" );
                break;

            default:
                break;
            }   // End switch
        }
        else
            done = true;

    }   // End while
}

bool ApplicationPrivate::initSdl2()
{
    //
    // initialize SDL 2
    //

    int error = SDL_Init(SDL_INIT_EVERYTHING);
    if ( error ) {
        SDL_LogError( SDL_LOG_CATEGORY_APPLICATION, "SDL_Init error:%s", SDL_GetError() );
        return false;
    }


    //
    // create GL context
    //

    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
    SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 1 );

    SDL_GL_SetSwapInterval( 1 );

    mainWindow = SDL_CreateWindow( name.toUtf8(),
                                  SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                  screenSize.getX(), screenSize.getY(),
                                  SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

    mainGLContext = SDL_GL_CreateContext( mainWindow );
    if ( mainGLContext == NULL ) {
        SDL_LogError( SDL_LOG_CATEGORY_APPLICATION, "SDL_GL_CreateContext error:%s\n", SDL_GetError() );
        return false;
    }


    int majorVersion = 0, minorVersion = 0;
    SDL_GL_GetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, &majorVersion );
    SDL_GL_GetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, &minorVersion );
    SDL_LogInfo( SDL_LOG_CATEGORY_APPLICATION, "OpenGL version: %d.%d\n", majorVersion, minorVersion );


    //
    // initialize gl context
    //

    glClearColor( 0.5f, 0.5f, 0.5f, 1.0f );
    glViewport( 0, 0, (GLsizei)screenSize.getX(), (GLsizei)screenSize.getY());
    glMatrixMode( GL_PROJECTION );
    glLoadIdentity();
//    gluPerspective( 65.0, (GLfloat)w / (GLfloat)h, 1.0, 20.0 );
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    glTranslatef( 0.0, 0.0, -5.0 );


    //
    // initialize paint context
    //
    mainPaintContext.getBody()->skiaContext = GrContext::Create( kOpenGL_GrBackend, 0 );

    GrBackendRenderTargetDesc desc;
    desc.fWidth = screenSize.getX();
    desc.fHeight = screenSize.getY();
    desc.fConfig = kSkia8888_GrPixelConfig;
    desc.fOrigin = kBottomLeft_GrSurfaceOrigin;
    desc.fSampleCnt = 1;
    desc.fStencilBits = 0;
    desc.fRenderTargetHandle = 0;  // assume default framebuffer

    mainPaintContext.getBody()->skiaSurface = SkSurface::MakeFromBackendRenderTarget( mainPaintContext.getBody()->skiaContext, desc, NULL).release();
    mainPaintContext.getBody()->skiaCanvas = mainPaintContext.getBody()->skiaSurface->getCanvas();

    SDL_GL_MakeCurrent( mainWindow, mainGLContext );
    int displayCount = SDL_GetNumVideoDisplays();
    for ( int displayNum = 0; displayNum < displayCount; displayNum++ ) {
        SDL_LogInfo( SDL_LOG_CATEGORY_APPLICATION, "Display[%d]:%s\n", displayNum, SDL_GetDisplayName( displayNum ) );

        float ddpi, hdpi, vdpi;
        int result = SDL_GetDisplayDPI( 0, &ddpi, &hdpi, &vdpi );
        if( result == 0 )
        {
            screenDpi = ddpi;
            SDL_LogInfo( SDL_LOG_CATEGORY_APPLICATION, "Screen Dpi: %f\n", screenDpi );
        } else {
            SDL_LogInfo( SDL_LOG_CATEGORY_APPLICATION, "GetDisplayDpi error[%d]:%s\n", result, SDL_GetError() );
        }
    }

    return true;
}

void ApplicationPrivate::closeSdl2()
{
    SDL_GL_DeleteContext(mainGLContext);
    SDL_DestroyWindow(mainWindow);
}

void ApplicationPrivate::terminate( int resultCode )
{
    state = Application::terminatingState;
    this->resultCode = resultCode;

    SDL_Event quitEvent;
    quitEvent.type = SDL_QUIT;
    quitEvent.quit.timestamp = 0;
    SDL_PushEvent( &quitEvent );
}

void ApplicationPrivate::paint()
{
    uint64_t t0 = Time::microSeconds();

    // erase screen surface
    glClear( GL_COLOR_BUFFER_BIT );


    // draw top view and their children
    if ( topView ) {

        viewAnimated = topView->updatePaint( t0 );

        if ( topView->isLayoutRequested() )
            topView->layout(screenSize);

        topView->paint( mainPaintContext );
        mainPaintContext.getBody()->skiaCanvas->flush();
    }
    else
        viewAnimated = false;

    uint64_t t30 = Time::microSeconds();
    SDL_LogInfo( SDL_LOG_CATEGORY_APPLICATION, "drawing duration: %d us", (int)(t30 - t0) );

    // flip this new surface
    SDL_GL_SwapWindow( mainWindow );
}

void ApplicationPrivate::convertSdlKeyDownEvent( const SDL_KeyboardEvent & keyEvent )
{
}

void ApplicationPrivate::convertSdlTexInputEvent( const SDL_TextInputEvent & textEvent )
{
    pushEvent( boost::make_shared<KeyboardEvent>( textEvent.timestamp, String::fromUtf8( textEvent.text ) ) );
}

void ApplicationPrivate::convertSdlMouseButtonDown( const SDL_MouseButtonEvent & buttonEvent )
{
    Point2d position(buttonEvent.x, buttonEvent.y);
    pushEvent( boost::make_shared<TouchEvent>( Event::touchPressedEventType,
                                               buttonEvent.timestamp,
                                               position, position,
                                               1.0f, // presure
                                               0 // fingerId
                                             ) );
}

void ApplicationPrivate::convertSdlMouseButtonUp( const SDL_MouseButtonEvent & buttonEvent )
{
    Point2d position(buttonEvent.x, buttonEvent.y);
    pushEvent( boost::make_shared<TouchEvent>( Event::touchUnpressedEventType,
                                               buttonEvent.timestamp,
                                               position, position,
                                               0.0f, // presure
                                               0 // fingerId
                                             ) );
}

void ApplicationPrivate::convertSdlMouseMotion( const SDL_MouseMotionEvent & motionEvent )
{
    Point2d position(motionEvent.x, motionEvent.y);
    pushEvent( boost::make_shared<TouchEvent>( Event::touchMovedEventType,
                                               motionEvent.timestamp,
                                               position, position,
                                               1.0f, // presure
                                               0 // fingerId
                                             ) );
}

const String ApplicationPrivate::findAssetFilePath( const String & fileName ) const
{
    return fileName;
}


//=================================================================================================================================
ApplicationStateChangedEvent::ApplicationStateChangedEvent(uint32_t timestamp, Application::State appState )
    : Event( appStateChangedEventType, timestamp )
    , appState( appState )
{
}

}
