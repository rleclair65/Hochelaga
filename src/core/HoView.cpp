/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoViewPrivate.h"
#include "HoApplication.h"
#include "HoPaintContextPrivate.h"

#include <SDL.h>
#include <algorithm>
#include <assert.h>
#include <boost/make_shared.hpp>

namespace Hochelaga {

static const Scalar kMmPerIn = 25.4f;
static const Scalar kPtPerIn = 72.0f;

ViewWPtr ViewPrivate::focusedView;

//=================================================================================================================================


View::View( const MeasureInfo & measureInfo, Alignment alignment )
    : Handle<ViewPrivate>( boost::make_shared<ViewPrivate>( this, measureInfo, alignment) )
{
}

View::View(const boost::shared_ptr<ViewPrivate> & body)
    : Handle<ViewPrivate>(body)
{

}

Scalar View::getTransparency() const
{
    return getBody()->getTransparency();
}

void View::setTransparency( Scalar newTransparency )
{
    getBody()->setTransparency(newTransparency);
}

const Hochelaga::Point2d & View::getOrigin() const
{
    return getBody()->getOrigin();
}

void View::setOrigin(const Point2d & newOrigin)
{
    getBody()->setOrigin(newOrigin);
}

Scalar View::getZ() const
{
    return getBody()->getZ();
}

void View::setZ( Scalar newZ )
{
    getBody()->setZ(newZ);
}

const Point2d & View::getSize() const
{
    return getBody()->getSize();
}

void View::setSize(const Point2d & newSize)
{
    getBody()->setSize(newSize);
}

View::Alignment View::getAlignment() const
{
    return getBody()->getAlignment();
}

const Rect2d & View::getMargins() const
{
    return getBody()->getMargins();
}

void View::setMargins( const Rect2d & newMargins )
{
    getBody()->setMargins(newMargins);
}

const Rect2d & View::getPadding() const
{
    return getBody()->getPadding();
}

void View::setPadding( const Rect2d & newPadding )
{
    getBody()->setPadding(newPadding);
}

const Color & View::getBackgroundColor() const
{
    return getBody()->getBackgroundColor();
}

void View::setBackgroundColor( const Color & newColor )
{
    getBody()->setBackgroundColor(newColor);
}

void View::addChildView( const ViewSPtr & childView )
{
    getBody()->addChildView(childView);
}

void View::removeChildView( const ViewSPtr & childView )
{
    getBody()->removeChildView(childView);
}

size_t View::getChildViewCount() const
{
    return getBody()->getChildViewCount();
}

ViewSPtr View::getChildView(size_t ndx) const
{
    return getBody()->getChildView(ndx);
}

ViewSPtr View::getParentView()
{
    return getBody()->getParentView();
}


void View::requestPaint()
{
    getBody()->requestPaint();
}

bool View::updatePaint( TimeInMicroSeconds currentTime )
{
    return getBody()->updatePaint(currentTime);
}

void View::paint( const PaintContext & context )
{
    getBody()->paint(context);
}

void View::requestLayout()
{
    getBody()->requestLayout();
}

bool View::isLayoutRequested() const
{
    return getBody()->isLayoutRequested();
}

void View::layout(const Hochelaga::Point2d & newSize)
{
    getBody()->layout(newSize);
}

const Point2d View::calculatePreferredSize()
{
    return getBody()->calculatePreferredSize();
}

void View::requestFocus()
{
    getBody()->requestFocus();
}

ViewSPtr View::getFocusedView()
{
    return ViewPrivate::getFocusedView();
}

void View::onEvent( const EventSPtr & event )
{
    getBody()->onEvent(event);
}

const Point2d View::mapFromParent(const Point2d & parentPoint)
{
    return getBody()->mapFromParent(parentPoint);
}

const Point2d View::mapToParent(const Point2d & point)
{
    return getBody()->mapToParent(point);
}

const Point2d View::mapFromScreen(const Point2d & screenPoint)
{
    return getBody()->mapFromScreen(screenPoint);
}

const Point2d View::mapToScreen(const Hochelaga::Point2d &point)
{
    return getBody()->mapToScreen(point);
}

//=================================================================================================================================
ViewPrivate::ViewPrivate( View * handle, const View::MeasureInfo & measureInfo, View::Alignment alignment )
    : Body<View>(handle)
    , tranparency( 0.0f )
    , origin( 0.0f, 0.0f )
    , z( 0.0f )
    , size(0.0f, 0.0f)
    , alignment( alignment )
    , measureInfo( measureInfo )
    , layoutRequested( true )
    , paintRequested( true )
    , margins( 0.0f, 0.0f, 0.0f, 0.0f )
{
}

void ViewPrivate::addChildView( const ViewSPtr & childView )
{
    assert( childView );
    assert( ! childView->getParentView() );

    childView->getBody()->setParentView( getHandle()->shared_from_this() );
    childViewVector.push_back( childView );
}

void ViewPrivate::removeChildView( const ViewSPtr & childView )
{
    assert( childView );
    assert( childView->getParentView() );

    ChildViewVector::iterator iterFound = std::find( childViewVector.begin(), childViewVector.end(), childView );
    if ( iterFound == childViewVector.end() )
        return;

    childView->getBody()->setParentView( ViewSPtr() );
    childViewVector.erase( iterFound );
}

size_t ViewPrivate::getChildViewCount() const
{
    return childViewVector.size();
}

ViewSPtr ViewPrivate::getChildView(size_t ndx) const
{
    return childViewVector[ ndx ];
}

void ViewPrivate::setOrigin(const Point2d & newOrigin)
{
    origin = newOrigin;
}

ViewSPtr ViewPrivate::getParentView()
{
    return parentView.lock();
}

void ViewPrivate::setParentView( const ViewSPtr & newParentView )
{
    parentView = newParentView;
}

bool ViewPrivate::updatePaint( TimeInMicroSeconds currentTime )
{
    bool result = false;
    paintRequested = false;

    for( auto currView : childViewVector )
    {
        if ( currView->updatePaint( currentTime ) )
        {
            currView->requestPaint();
            result = true;
        }
    }

    return result;
}

void ViewPrivate::requestLayout()
{
    if ( layoutRequested )
        return;

    layoutRequested = true;
    ViewSPtr parentView = getParentView();
    if ( parentView )
        parentView->requestLayout();
}

void ViewPrivate::layout(const Point2d & newSize)
{
    size = newSize;
    layoutRequested = false;
    paintRequested = true;

    for ( auto childView : childViewVector )
    {
        childView->setOrigin(Point2d(0.0f, 0.0f));

        Point2d preferredSize = childView->calculatePreferredSize();
        childView->layout(preferredSize);
    }
}

Scalar ViewPrivate::toPixel( const Dimension & dimension ) const
{
    switch( dimension.getUnit() )
    {
    case Dimension::pxUnit:
    case Dimension::dipUnit:
        return dimension.getValue();

    case Dimension::inUnit:
        return dimension.getValue() / Application::getInstance()->getScreenDpi();

    case Dimension::mmUnit:
        return dimension.getValue() / Application::getInstance()->getScreenDpi() * kMmPerIn;

    case Dimension::ptUnit:
        return dimension.getValue() / Application::getInstance()->getScreenDpi() * kPtPerIn;

    case Dimension::screenWidthRelativeUnit:
        return dimension.getValue() * Application::getInstance()->getScreenSize().getX();

    case Dimension::screenHeightRelativeUnit:
        return dimension.getValue() * Application::getInstance()->getScreenSize().getY();

    case Dimension::parentWidthRelativeUnit:
        return dimension.getValue() * parentView.lock()->getSize().getX();

    case Dimension::parentHeightRelativeUnit:
        return dimension.getValue() * parentView.lock()->getSize().getY();

    default:
        assert( false );
        return 0;
    }
}

const Point2d ViewPrivate::calculatePreferredSize()
{
    return Point2d( toPixel( measureInfo.preferredWidth ),  toPixel( measureInfo.preferredHeight ) );
}

const Point2d ViewPrivate::clampSize( const Point2d & size ) const
{
    Point2d result = size;

    if ( measureInfo.minWidth.getUnit() != Dimension::unknownUnit ) {
        Scalar minWidth = toPixel( measureInfo.minWidth );
        if( result.getX() < minWidth )
            result.setX( minWidth );
    }

    if ( measureInfo.maxWidth.getUnit() != Dimension::unknownUnit ) {
        Scalar maxWidth = toPixel( measureInfo.maxWidth );
        if( result.getX() > maxWidth )
            result.setX( maxWidth );
    }

    if ( measureInfo.minHeight.getUnit() != Dimension::unknownUnit ) {
        Scalar minHeight = toPixel( measureInfo.minHeight );
        if( result.getY() < minHeight )
            result.setY( minHeight );
    }

    if ( measureInfo.maxHeight.getUnit() != Dimension::unknownUnit ) {
        Scalar maxHeight = toPixel( measureInfo.maxWidth );
        if( result.getY() > maxHeight )
            result.setY( maxHeight );
    }

    return result;
}

void ViewPrivate::requestPaint()
{
    if ( paintRequested )
        return;

    paintRequested = true;
    ViewSPtr parentView = getParentView();
    if ( parentView )
        parentView->requestPaint();
}

void ViewPrivate::paint( const PaintContext & context )
{
    paintBackground( context );
    paintChildren( context );
}

void ViewPrivate::paintBackground( const PaintContext & context )
{
    if ( ! backgroundColor.isTransparent() )
        context.getBody()->skiaCanvas->clear( backgroundColor.toARGB() );
}

void ViewPrivate::paintChildren( const PaintContext & context )
{
    for ( auto childView : childViewVector ) {

        context.getBody()->skiaCanvas->save();

        const Point2d & childOrigin = childView->getOrigin();
        context.getBody()->skiaCanvas->translate( SkFloatToScalar( childOrigin.getX() ), SkFloatToScalar( childOrigin.getY() ) );

        const Point2d & childSize = childView->getSize();
        context.getBody()->skiaCanvas->clipRect( SkRect::MakeWH( childSize.getX(), childSize.getY() ) );

        childView->paint( context );

        context.getBody()->skiaCanvas->restore();
    }
}

void ViewPrivate::onEvent( const EventSPtr & event )
{
    switch( event->getType() )
    {
    case Event::appStateChangedEventType:
        onAppStateChangedEvent( * boost::dynamic_pointer_cast<ApplicationStateChangedEvent>( event ).get() );
        break;

    case Event::keyboardEventType:
        onKeyboardEvent( *boost::dynamic_pointer_cast<KeyboardEvent>( event ).get() );
        break;

    case Event::touchPressedEventType:
    case Event::touchUnpressedEventType:
    case Event::touchEnteredEventType:
    case Event::touchLeavedEventType:
    case Event::touchMovedEventType:
    case Event::touchCancelledEventType:
        onTouchEvent( *boost::dynamic_pointer_cast<TouchEvent>( event ).get() );
        break;

    default:
        break;
    }
}

void ViewPrivate::onAppStateChangedEvent( const ApplicationStateChangedEvent & appStateChangedEvent )
{
    for ( auto childView : childViewVector )
        childView->getBody()->onAppStateChangedEvent( appStateChangedEvent );
}

void ViewPrivate::onKeyboardEvent( const KeyboardEvent & keyboardEvent )
{
    for ( auto childView : childViewVector )
        childView->getBody()->onKeyboardEvent( keyboardEvent );
}

bool ViewPrivate::onTouchEvent(const TouchEvent & touchEvent )
{
    for ( auto childView : childViewVector )
    {
        Point2d childPosition = childView->mapFromParent(touchEvent.getViewPosition());
        if( (childPosition.getX() >= 0) && (childPosition.getY() >= 0) )
        {
            Point2d childSize = childView->getSize();
            if ( (childPosition.getX() < childSize.getX()) && (childPosition.getY() < childSize.getY()) )
            {
                TouchEvent childTouchEvent(touchEvent.getType(),
                                       touchEvent.getTimestamp(),
                                       touchEvent.getScreenPosition(),
                                       childPosition,
                                       touchEvent.getPression(),
                                       touchEvent.getFingerId()
                                    );
                if(childView->getBody()->onTouchEvent(childTouchEvent))
                    return true; // consumed
            }
        }
    }

    return false; // not consumed
}

void ViewPrivate::requestFocus()
{
}

void ViewPrivate::setSize(const Point2d & newSize)
{
    size = newSize;
}

const Point2d ViewPrivate::mapFromParent(const Point2d & parentPoint)
{
    return parentPoint - origin;
}

const Point2d ViewPrivate::mapToParent(const Point2d & point)
{
    return point + origin;
}

const Point2d ViewPrivate::mapFromScreen(const Point2d & screenPoint)
{
    ViewSPtr parentView = getParentView();
    if (parentView)
        return parentView->mapFromScreen(mapFromParent(screenPoint));
    else
        return parentView->mapFromParent(screenPoint);
}

const Point2d ViewPrivate::mapToScreen(const Point2d & point)
{
    ViewSPtr parentView = getParentView();
    if (parentView)
        return parentView->mapToScreen(mapToParent(point));
    else
        return parentView->mapToParent(point);
}


//================================================================================================================================
View::MeasureInfo::MeasureInfo()
{
}

const View::MeasureInfo View::MeasureInfo::fromPreferredWH( const Dimension & preferredWidth,
                                                            const Dimension & preferredHeight
                                                            )
{
    View::MeasureInfo result;

    result.minWidth = result.maxWidth = result.preferredWidth = preferredWidth;
    result.minHeight = result.maxHeight = result.preferredHeight = preferredHeight;

    return result;
}

//================================================================================================================================
KeyboardEvent::KeyboardEvent( uint32_t timestamp, const String & text )
    : Event( keyboardEventType, timestamp )
    , text( text )
{
}


//================================================================================================================================
TouchEvent::TouchEvent( Type type,
                        uint32_t timestamp,
                        const Point2d & screenPosition,
                        const Point2d & viewPosition,
                        Scalar pression,
                        int fingerId)
    : Event(type, timestamp)
    , screenPosition(screenPosition)
    , viewPosition(viewPosition)
    , pression(pression)
    , fingerId(fingerId)
{
}

} // namespace Hochelaga

