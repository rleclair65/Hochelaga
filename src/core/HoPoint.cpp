/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoPoint.h"

namespace Hochelaga {

const Point2d Point2d::operator + ( const Point2d & rhs ) const
{
    return Point2d( x + rhs.x, y + rhs.y );
}

const Point2d Point2d::operator - ( const Point2d & rhs ) const
{
    return Point2d( x - rhs.x, y - rhs.y );
}

}

