/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoTypes.h"
#include "HoHandleBody.h"
#include "HoEvent.h"
#include "HoString.h"
#include "HoDimension.h"
#include "HoRect.h"
#include "HoMatrix.h"
#include "HoColor.h"
#include "HoTime.h"
#include "HoPaintContext.h"

#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace Hochelaga {

class View;
typedef boost::shared_ptr<View> ViewSPtr;
typedef boost::weak_ptr<View> ViewWPtr;

class KeyboardEvent;
typedef boost::shared_ptr<KeyboardEvent> KeyboardEventSPtr;

class TouchEvent;
typedef boost::shared_ptr<TouchEvent> TouchEventSPtr;

class ApplicationStateChangedEvent;
typedef boost::shared_ptr<ApplicationStateChangedEvent> ApplicationStateChangedEventSPtr;

class ViewPrivate;

/**
 * @brief The View class
 */
class View
    : public boost::enable_shared_from_this<View>
    , public Handle<ViewPrivate>
{
public:
    enum Alignment {
        noAlignment       = 0x00,

        leftAlignment           = 0x01,
        centerAlignment         = 0x02,
        rightAlignment          = 0x03,
        horizontalAlignmentMask = 0x0f,

        topAlignment          = 0x10,
        vcenterAlignment      = 0x20,
        bottomAlignment       = 0x30,
        verticalAlignmentMask = 0x0f
    };

    class MeasureInfo
    {
    public:

    public:
        MeasureInfo();

        static const MeasureInfo fromPreferredWH( const Dimension & preferredWidth, const Dimension & preferredHeight );

        Dimension preferredWidth;
        Dimension minWidth;
        Dimension maxWidth;

        Dimension preferredHeight;
        Dimension minHeight;
        Dimension maxHeight;
    };

public:
    View( const MeasureInfo & measureInfo, Alignment alignment );

    Scalar getTransparency() const;
    void setTransparency(Scalar newTransparency );

    const Point2d & getOrigin() const;
    void setOrigin(const Point2d & newOrigin);

    Scalar getZ() const;
    void setZ( Scalar newZ );

    const Point2d & getSize() const;
    void setSize( const Point2d & newSize);

    Alignment getAlignment() const;

    const Rect2d & getMargins() const;
    void setMargins( const Rect2d & newMargins );

    const Rect2d & getPadding() const;
    void setPadding( const Rect2d & newPadding );

    const Color & getBackgroundColor() const;
    void setBackgroundColor( const Color & newColor );

    void addChildView( const ViewSPtr & childView );
    void removeChildView( const ViewSPtr & childView );
    size_t getChildViewCount() const;
    ViewSPtr getChildView(size_t ndx) const;

    ViewSPtr getParentView();


    void requestPaint();
    bool updatePaint( TimeInMicroSeconds currentTime );
    void paint( const PaintContext & context );

    void requestLayout();
    bool isLayoutRequested() const;
    void layout(const Point2d & newSize);
    const Point2d calculatePreferredSize();

    void requestFocus();
    static ViewSPtr getFocusedView();

    void onEvent( const EventSPtr & event );

    const Point2d mapFromParent(const Point2d & parentPoint);
    const Point2d mapToParent(const Point2d & point);
    const Point2d mapFromScreen(const Point2d & screenPoint);
    const Point2d mapToScreen(const Point2d & point);


protected:
    View(const boost::shared_ptr<ViewPrivate> & body);
};

class KeyboardEvent
    : public Event
{
public:
    KeyboardEvent( uint32_t timestamp, const String & text );

    String getText() const  { return text; }


private:
    String text;
};

class TouchEvent
    : public Event
{
public:
    TouchEvent( Type type,
                uint32_t timestamp,
                const Point2d & screenPosition,
                const Point2d & viewPosition,
                Scalar pression,
                int fingerId);

    const Point2d getScreenPosition() const  { return screenPosition; }
    const Point2d getViewPosition() const  { return viewPosition; }
    Scalar getPression() const        { return pression; }
    int getFingerId() const           { return fingerId; }

private:
    Point2d screenPosition;
    Point2d viewPosition;
    Scalar pression;
    int fingerId;
};

} // namespace Hochelaga

