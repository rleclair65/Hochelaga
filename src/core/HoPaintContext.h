/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */
#pragma once

#include "HoPaintContext.h"
#include "HoHandleBody.h"

namespace Hochelaga {

class PaintContextPrivate;

class PaintContext
    : public Handle<PaintContextPrivate>
{
public:
    PaintContext();
};

}

