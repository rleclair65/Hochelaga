#pragma once

#include <stdint.h>

namespace Hochelaga {

typedef uint64_t TimeInMicroSeconds;

class Time
{
public:
    Time();

    static TimeInMicroSeconds microSeconds();
};

}
