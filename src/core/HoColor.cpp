/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoColor.h"

namespace Hochelaga {

// Web Color table https://fr.wikipedia.org/wiki/Couleur_du_Web
const Color Color::kAliceBlue = Color::fromRGB( 0xF0F8FF );
const Color Color::kAntiqueWhite = Color::fromRGB( 0xFAEBD7 );
const Color Color::kAqua = Color::fromRGB( 0x00FFFF );
const Color Color::kAquamarine = Color::fromRGB( 0x7FFFD4 );
const Color Color::kAzure = Color::fromRGB( 0xF0FFFF );
const Color Color::kBeige = Color::fromRGB( 0xF5F5DC );
const Color Color::kBisque = Color::fromRGB( 0xFFE4C4 );
const Color Color::kBlack = Color::fromRGB( 0x000000 );
const Color Color::kBlanchedAlmond = Color::fromRGB( 0xFFEBCD );
const Color Color::kBlue = Color::fromRGB( 0x0000FF );
const Color Color::kBlueViolet = Color::fromRGB( 0x8A2BE2 );
const Color Color::kBrown = Color::fromRGB( 0xA52A2A );
const Color Color::kBurlyWoord = Color::fromRGB( 0xDEB887 );
const Color Color::kCadatBlue = Color::fromRGB( 0x5F9EA0 );
const Color Color::kChartreuse = Color::fromRGB( 0x7FFF00 );
const Color Color::kChocolate = Color::fromRGB( 0xD2691E );
const Color Color::kCoral = Color::fromRGB( 0xFF7F50 );
const Color Color::kCornFlowerBlue = Color::fromRGB( 0x6495ED );
const Color Color::kCornSilk = Color::fromRGB( 0xFFF8DC );
const Color Color::kCrimson = Color::fromRGB( 0xDC143C );
const Color Color::kCyan = Color::fromRGB( 0x00FFFF );
const Color Color::kDarkBlue = Color::fromRGB( 0x00008B );
const Color Color::kDarkCyan = Color::fromRGB( 0x008B8B );
const Color Color::kDarkGoldenRod = Color::fromRGB( 0xB8860B );
const Color Color::kDarlGray = Color::fromRGB( 0xA9A9A9 );
const Color Color::kDarkGreen = Color::fromRGB( 0x006400 );
const Color Color::kDarkKhaki = Color::fromRGB( 0xBDB76B );
const Color Color::kDarlMagenta = Color::fromRGB( 0x8B008B );
const Color Color::kDakkOliveGreen = Color::fromRGB( 0x556B2F );
const Color Color::kDarkOrange = Color::fromRGB( 0xFF8C00 );
const Color Color::kDarkOrchid = Color::fromRGB( 0x9932CC );
const Color Color::kDarkRed = Color::fromRGB( 0x8B0000 );
const Color Color::kDarkSalmon = Color::fromRGB( 0xE9967A );
const Color Color::kDarkSeaGreen = Color::fromRGB( 0x8FBC8F );
const Color Color::kDarkSlateBlue = Color::fromRGB( 0x483D8B );
const Color Color::kDarkSlateGray = Color::fromRGB( 0x2F4F4F );
const Color Color::kDarkTurtoise = Color::fromRGB( 0x00CED1 );
const Color Color::kDarkViolet = Color::fromRGB( 0x9400D3 );
const Color Color::kDeepPink = Color::fromRGB( 0xFF1493 );
const Color Color::kDeppSkyBlue = Color::fromRGB( 0x00BFFF );
const Color Color::kDimGray = Color::fromRGB( 0x696969 );
const Color Color::kDodgetBlue = Color::fromRGB( 0x1E90FF );
const Color Color::kFireBrick = Color::fromRGB( 0xB22222 );
const Color Color::kFloralWhite = Color::fromRGB( 0xFFFAF0 );
const Color Color::kForestGreen = Color::fromRGB( 0x228B22 );
const Color Color::kFuchsia = Color::fromRGB( 0xFF00FF );
const Color Color::kGainsboro = Color::fromRGB( 0xDCDCDC );
const Color Color::kGhostWhite = Color::fromRGB( 0xF8F8FF );
const Color Color::kGold = Color::fromRGB( 0xFFD700 );
const Color Color::kGoldenRod = Color::fromRGB( 0xDAA520 );
const Color Color::kGrey = Color::fromRGB( 0x808080 );
const Color Color::kGreen = Color::fromRGB( 0x008000 );
const Color Color::kGreenYellow = Color::fromRGB( 0xADFF2F );
const Color Color::kHoneyDew = Color::fromRGB( 0xF0FFF0 );
const Color Color::kHotPink = Color::fromRGB( 0xFF69B4 );
const Color Color::kIndianRed = Color::fromRGB( 0xCD5C5C );
const Color Color::kIndigo = Color::fromRGB( 0x4B0082 );
const Color Color::kIvory = Color::fromRGB( 0xFFFFF0 );
const Color Color::kKhaki = Color::fromRGB( 0xF0E68C );
const Color Color::kLavender = Color::fromRGB( 0xE6E6FA );
const Color Color::kLavendBlush = Color::fromRGB( 0xFFF0F5 );
const Color Color::kLawnGreen = Color::fromRGB( 0x7CFC00 );
const Color Color::kLemonChiffon = Color::fromRGB( 0xFFFACD );
const Color Color::kLighBlue = Color::fromRGB( 0xADD8E6 );
const Color Color::kLightCoral = Color::fromRGB( 0xF08080 );
const Color Color::kLightCyan = Color::fromRGB( 0xE0FFFF );
const Color Color::kLighGoldenRodYellow = Color::fromRGB( 0xFAFAD2 );
const Color Color::kLightGreen = Color::fromRGB( 0x90EE90 );
const Color Color::kLightGray = Color::fromRGB( 0xD3D3D3 );
const Color Color::kLightGrey = Color::fromRGB( 0xD3D3D3 );
const Color Color::kLightPink = Color::fromRGB( 0xFFB6C1 );
const Color Color::kLightSalmon = Color::fromRGB( 0xFFA07A );
const Color Color::kLightSeaGreen = Color::fromRGB( 0x20B2AA );
const Color Color::kLightSkyBlue = Color::fromRGB( 0x87CEFA );
const Color Color::kLightSlateGray = Color::fromRGB( 0x778899 );
const Color Color::kLightSteelBlue = Color::fromRGB( 0xB0C4DE );
const Color Color::kLigthYellow = Color::fromRGB( 0xFFFFE0 );
const Color Color::kLime = Color::fromRGB( 0x00FF00 );
const Color Color::kLimeGreen = Color::fromRGB( 0x32CD32 );
const Color Color::kLinen = Color::fromRGB( 0xFAF0E6 );
const Color Color::kMagenta = Color::fromRGB( 0xFF00FF );
const Color Color::kMaroon = Color::fromRGB( 0x800000 );
const Color Color::kMediumAquamarine = Color::fromRGB( 0x66CDAA );
const Color Color::kMediumBlue = Color::fromRGB( 0x0000CD );
const Color Color::kMediumOrchid = Color::fromRGB( 0xBA55D3 );
const Color Color::kMediumPurple = Color::fromRGB( 0x9370DB );
const Color Color::kMediumSeaGreen = Color::fromRGB( 0x3CB371 );
const Color Color::kMediumSlateBlue = Color::fromRGB( 0x7B68EE );
const Color Color::kMediumSpringGreen = Color::fromRGB( 0x00FA9A );
const Color Color::kMediumTurquoise = Color::fromRGB( 0x48D1CC );
const Color Color::kMediumVioletRed = Color::fromRGB( 0xC71585 );
const Color Color::kMidnigthBlue = Color::fromRGB( 0x191970 );
const Color Color::kMingCream = Color::fromRGB( 0xF5FFFA );
const Color Color::kMistyRose = Color::fromRGB( 0xFFE4E1 );
const Color Color::kMoccasin = Color::fromRGB( 0xFFE4B5 );
const Color Color::kNavajoWhite = Color::fromRGB( 0xFFDEAD );
const Color Color::kNavy = Color::fromRGB( 0x000080 );
const Color Color::koldLace = Color::fromRGB( 0xFDF5E6 );
const Color Color::kOlive = Color::fromRGB( 0x808000 );
const Color Color::kOliveDrab = Color::fromRGB( 0x6B8E23 );
const Color Color::kOrange = Color::fromRGB( 0xFFA500 );
const Color Color::kOrangeRed = Color::fromRGB( 0xFF4500 );
const Color Color::kOrchid = Color::fromRGB( 0xDA70D6 );
const Color Color::kPaleGordenRod = Color::fromRGB( 0xEEE8AA );
const Color Color::kPaleGreen = Color::fromRGB( 0x98FB98 );
const Color Color::kPalteTurquoise = Color::fromRGB( 0xAFEEEE );
const Color Color::kPaleVioletRed = Color::fromRGB( 0xDB7093 );
const Color Color::kPapayWhip = Color::fromRGB( 0xFFEFD5 );
const Color Color::kPeachPuff = Color::fromRGB( 0xFFDAB9 );
const Color Color::kPeru = Color::fromRGB( 0xCD853F );
const Color Color::kPink = Color::fromRGB( 0xFFC0CB );
const Color Color::kPlum = Color::fromRGB( 0xDDA0DD );
const Color Color::kPowderBlue = Color::fromRGB( 0xB0E0E6 );
const Color Color::kPurple = Color::fromRGB( 0x800080 );
const Color Color::kRed = Color::fromRGB( 0xFF0000 );
const Color Color::kRosyBrown = Color::fromRGB( 0xBC8F8F );
const Color Color::kRoyalBlue = Color::fromRGB( 0x4169E1 );
const Color Color::kSaddleBrown = Color::fromRGB( 0x8B4513 );
const Color Color::kSalmon = Color::fromRGB( 0xFA8072 );
const Color Color::kSandyBrown = Color::fromRGB( 0xF4A460 );
const Color Color::kSeaGreen = Color::fromRGB( 0x2E8B57 );
const Color Color::kSeaShell = Color::fromRGB( 0xFFF5EE );
const Color Color::kSienna = Color::fromRGB( 0xA0522D );
const Color Color::kSilver = Color::fromRGB( 0xC0C0C0 );
const Color Color::kSkyBlue = Color::fromRGB( 0x87CEEB );
const Color Color::kSlateBlue = Color::fromRGB( 0x6A5ACD );
const Color Color::kSlateGray = Color::fromRGB( 0x708090 );
const Color Color::kSnow = Color::fromRGB( 0xFFFAFA );
const Color Color::kSpringGreen = Color::fromRGB( 0x00FF7F );
const Color Color::kSteelBlue = Color::fromRGB( 0x4682B4 );
const Color Color::kTan = Color::fromRGB( 0xD2B48C );
const Color Color::kTeal = Color::fromRGB( 0x008080 );
const Color Color::kThistle = Color::fromRGB( 0xD8BFD8 );
const Color Color::kTomato = Color::fromRGB( 0xFF6347 );
const Color Color::kTurquoise = Color::fromRGB( 0x40E0D0 );
const Color Color::kViolet = Color::fromRGB( 0xEE82EE );
const Color Color::kWheat = Color::fromRGB( 0xF5DEB3 );
const Color Color::kWhite = Color::fromRGB( 0xFFFFFF );
const Color Color::kWhiteSmoke = Color::fromRGB( 0xF5F5F5 );
const Color Color::kYellow = Color::fromRGB( 0xFFFF00 );
const Color Color::kYellowGreen = Color::fromRGB( 0x9ACD32 );

uint32_t Color::toARGB() const
{
    return ((uint32_t)(alpha * 255) << 24)
            + ((uint32_t)(red * 255) << 16)
            + ((uint32_t)(green * 255) << 8)
            + (uint32_t)(blue * 255);
}

const Color Color::fromRGB( uint32_t rgb )
{
    return Color( ((rgb >> 16) & 0xff) / 255.0f,
                  ((rgb >> 8) & 0xff) / 255.0f,
                  (rgb & 0xff) / 255.0f
                  );
}

}
