/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include <vector>

namespace Hochelaga {

class String;

class ByteArray
    : public std::vector<uint8_t>
{
public:
    ByteArray();
    ByteArray(const uint8_t * data, size_t size);

    operator const char * () const  { return (const char *)data(); }
    operator const void * () const  { return (const void *)data(); }

    static const ByteArray fromBuffer( const void * data, size_t size );

    static const ByteArray fromHex2String( const String & hex2String );
    const String toHex2String() const;
};

}

