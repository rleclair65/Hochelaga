/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoTypes.h"

#include <stdint.h>

namespace Hochelaga {

class Color
{
public:
    Color()
        : red( 0.0f )
        , green( 0.0f )
        , blue( 0.0f )
        , alpha( 0.0f )
    {
    }

    Color( Scalar red, Scalar green, Scalar blue, Scalar alpha = 1.0f )
        : red( red )
        , green( green )
        , blue( blue )
        , alpha( alpha )
    {
    }

    Scalar getRed() const   { return red; }
    Scalar getGreen() const { return green; }
    Scalar getBlue() const  { return blue; }
    Scalar getAlpha() const { return alpha; }

    uint32_t toARGB() const;
    static const Color fromRGB( uint32_t rgb );

    bool isTransparent() const  { return (alpha <= 0.0f); }
    bool isOpaque() const       { return (alpha >= 1.0f); }

    // Web Color table https://fr.wikipedia.org/wiki/Couleur_du_Web
    static const Color kAliceBlue;
    static const Color kAntiqueWhite;
    static const Color kAqua;
    static const Color kAquamarine;
    static const Color kAzure;
    static const Color kBeige;
    static const Color kBisque;
    static const Color kBlack;
    static const Color kBlanchedAlmond;
    static const Color kBlue;
    static const Color kBlueViolet;
    static const Color kBrown;
    static const Color kBurlyWoord;
    static const Color kCadatBlue;
    static const Color kChartreuse;
    static const Color kChocolate;
    static const Color kCoral;
    static const Color kCornFlowerBlue;
    static const Color kCornSilk;
    static const Color kCrimson;
    static const Color kCyan;
    static const Color kDarkBlue;
    static const Color kDarkCyan;
    static const Color kDarkGoldenRod;
    static const Color kDarlGray;
    static const Color kDarkGreen;
    static const Color kDarkKhaki;
    static const Color kDarlMagenta;
    static const Color kDakkOliveGreen;
    static const Color kDarkOrange;
    static const Color kDarkOrchid;
    static const Color kDarkRed;
    static const Color kDarkSalmon;
    static const Color kDarkSeaGreen;
    static const Color kDarkSlateBlue;
    static const Color kDarkSlateGray;
    static const Color kDarkTurtoise;
    static const Color kDarkViolet;
    static const Color kDeepPink;
    static const Color kDeppSkyBlue;
    static const Color kDimGray;
    static const Color kDodgetBlue;
    static const Color kFireBrick;
    static const Color kFloralWhite;
    static const Color kForestGreen;
    static const Color kFuchsia;
    static const Color kGainsboro;
    static const Color kGhostWhite;
    static const Color kGold;
    static const Color kGoldenRod;
    static const Color kGrey;
    static const Color kGreen;
    static const Color kGreenYellow;
    static const Color kHoneyDew;
    static const Color kHotPink;
    static const Color kIndianRed;
    static const Color kIndigo;
    static const Color kIvory;
    static const Color kKhaki;
    static const Color kLavender;
    static const Color kLavendBlush;
    static const Color kLawnGreen;
    static const Color kLemonChiffon;
    static const Color kLighBlue;
    static const Color kLightCoral;
    static const Color kLightCyan;
    static const Color kLighGoldenRodYellow;
    static const Color kLightGreen;
    static const Color kLightGray;
    static const Color kLightGrey;
    static const Color kLightPink;
    static const Color kLightSalmon;
    static const Color kLightSeaGreen;
    static const Color kLightSkyBlue;
    static const Color kLightSlateGray;
    static const Color kLightSteelBlue;
    static const Color kLigthYellow;
    static const Color kLime;
    static const Color kLimeGreen;
    static const Color kLinen;
    static const Color kMagenta;
    static const Color kMaroon;
    static const Color kMediumAquamarine;
    static const Color kMediumBlue;
    static const Color kMediumOrchid;
    static const Color kMediumPurple;
    static const Color kMediumSeaGreen;
    static const Color kMediumSlateBlue;
    static const Color kMediumSpringGreen;
    static const Color kMediumTurquoise;
    static const Color kMediumVioletRed;
    static const Color kMidnigthBlue;
    static const Color kMingCream;
    static const Color kMistyRose;
    static const Color kMoccasin;
    static const Color kNavajoWhite;
    static const Color kNavy;
    static const Color koldLace;
    static const Color kOlive;
    static const Color kOliveDrab;
    static const Color kOrange;
    static const Color kOrangeRed;
    static const Color kOrchid;
    static const Color kPaleGordenRod;
    static const Color kPaleGreen;
    static const Color kPalteTurquoise;
    static const Color kPaleVioletRed;
    static const Color kPapayWhip;
    static const Color kPeachPuff;
    static const Color kPeru;
    static const Color kPink;
    static const Color kPlum;
    static const Color kPowderBlue;
    static const Color kPurple;
    static const Color kRed;
    static const Color kRosyBrown;
    static const Color kRoyalBlue;
    static const Color kSaddleBrown;
    static const Color kSalmon;
    static const Color kSandyBrown;
    static const Color kSeaGreen;
    static const Color kSeaShell;
    static const Color kSienna;
    static const Color kSilver;
    static const Color kSkyBlue;
    static const Color kSlateBlue;
    static const Color kSlateGray;
    static const Color kSnow;
    static const Color kSpringGreen;
    static const Color kSteelBlue;
    static const Color kTan;
    static const Color kTeal;
    static const Color kThistle;
    static const Color kTomato;
    static const Color kTurquoise;
    static const Color kViolet;
    static const Color kWheat;
    static const Color kWhite;
    static const Color kWhiteSmoke;
    static const Color kYellow;
    static const Color kYellowGreen;

private:
    Scalar red;
    Scalar green;
    Scalar blue;
    Scalar alpha;
};

}
