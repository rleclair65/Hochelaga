/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoPaintContextPrivate.h"
#include <boost/make_shared.hpp>

namespace Hochelaga {

PaintContext::PaintContext()
    : Handle<PaintContextPrivate>( boost::make_shared<PaintContextPrivate>(this) )
{
}


//=================================================================================================================================

PaintContextPrivate::PaintContextPrivate(PaintContext * handle)
    : Body<PaintContext>( handle )
    , skiaContext(nullptr)
    , skiaSurface(nullptr)
    , skiaCanvas(nullptr)
{
}

PaintContextPrivate::~PaintContextPrivate()
{
//    SkCanvas * skiaCanvas;
    delete skiaSurface;
    delete skiaContext;
}

}
