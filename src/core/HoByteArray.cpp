/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoByteArray.h"

#include "HoString.h"

namespace Hochelaga {

ByteArray::ByteArray()
{
}

ByteArray::ByteArray(const uint8_t * data, size_t size)
    : std::vector<uint8_t>( data, data + size )
{
}

const ByteArray ByteArray::fromBuffer( const void * data, size_t size )
{
    return ByteArray( (uint8_t *)data, size);
}

const ByteArray ByteArray::fromHex2String( const String & hex2String )
{
    return ByteArray();
}

static inline wchar_t hexToChar( int hex )
{
    switch( hex ) {
    case 0x00:      return '0';
    case 0x01:      return '1';
    case 0x02:      return '2';
    case 0x03:      return '3';
    case 0x04:      return '4';
    case 0x05:      return '5';
    case 0x06:      return '6';
    case 0x07:      return '7';
    case 0x08:      return '8';
    case 0x09:      return '9';
    case 0x0a:      return 'a';
    case 0x0b:      return 'b';
    case 0x0c:      return 'c';
    case 0x0d:      return 'd';
    case 0x0e:      return 'e';
    case 0x0f:      return 'f';

    default:        return '?';
    }
}

const String ByteArray::toHex2String() const
{
    String result;

    for ( const_iterator iter = begin(); iter != end(); iter++ )
    {
        uint8_t curByte = *iter;
        result += hexToChar( (curByte >> 4) & 0x0f );
        result += hexToChar( curByte & 0x0f );
    }

    return result;
}

}
