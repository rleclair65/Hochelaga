/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */
#pragma once

#include "HoHandleBody.h"
#include "HoPaintContext.h"

//
// Skia library stuff
//
#include "SkCanvas.h"
#include "GrContext.h"
#include "SkSurface.h"
#include "SkStream.h"
#include "SkCodec.h"
#include "SkImageGenerator.h"


namespace Hochelaga {

class PaintContextPrivate
    : Body<PaintContext>
{
public:
    PaintContextPrivate(PaintContext * handle);
    ~PaintContextPrivate();

    GrContext * skiaContext;
    SkSurface * skiaSurface;
    SkCanvas * skiaCanvas;
};

}

