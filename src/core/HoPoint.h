/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoTypes.h"

namespace Hochelaga {

class Point2d
{
public:
    Point2d( Scalar x = 0.0f, Scalar y = 0.0f )
        : x( x )
        , y( y )
    {
    }

    const Point2d operator + (const Point2d & rhs ) const;
    const Point2d operator - (const Point2d & rhs ) const;

    Scalar getX() const       { return x; }
    void setX( Scalar newX )  { x = newX; }

    Scalar getY() const  { return y; }
    void setY( Scalar newY )  { y = newY; }


private:
    Scalar x;
    Scalar y;
};

class Point3d
{
public:
    Point3d( Scalar x = 0.0f, Scalar y = 0.0f, Scalar z = 0.0f )
        : x( x )
        , y( y )
        , z( z )
    {
    }

    const Point2d operator + (const Point2d & rhs ) const;

    Scalar getX() const       { return x; }
    void setX( Scalar newX )  { x = newX; }

    Scalar getY() const  { return y; }
    void setY( Scalar newY )  { y = newY; }

    Scalar getZ() const  { return z; }
    void setZ( Scalar newZ )  { z = newZ; }

private:
    Scalar x;
    Scalar y;
    Scalar z;
};
}

