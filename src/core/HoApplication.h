/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoView.h"
#include "HoEventQueue.h"
#include "HoString.h"
#include "HoHandleBody.h"

#include <vector>

namespace Hochelaga {

class ApplicationPrivate;

class Application
    : public Handle<ApplicationPrivate>
{
public:
    enum State
    {
        initializingState,
        foregroundState,
        backgroundState,
        terminatingState,
        terminatedState
    };

    typedef std::vector< String > ArguementList;


public:
    Application( const String & name, int argc, char * argv[] );
    ~Application();

    const ViewSPtr & getTopView() const;
    void setTopView(const ViewSPtr & newTopView);

    State getState() const;
    const Point2d & getScreenSize() const;
    Scalar getScreenDpi() const;

    const String findAssetFilePath( const String & fileName ) const;

    int run();
    void terminate( int resultCode = 0 );
    void paint();

    void pushEvent(const EventSPtr & event);
    void processAllEvents();

    static Application * getInstance()  { return instance; }

private:
    static Application * instance;
};

class ApplicationStateChangedEvent
    : public Event
{
public:
    ApplicationStateChangedEvent( uint32_t timestamp, Application::State appState );

    Application::State getAppState() const  { return appState; }

private:
    Application::State appState;
};

}
