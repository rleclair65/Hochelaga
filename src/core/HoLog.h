/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */
#pragma once

namespace Hochelaga {

class Log
{
public:
    static void verbose(const char *fmt, ...);
    static void info(const char *fmt, ...);
    static void warn(const char *fmt, ...);
    static void error(const char *fmt, ...);
};

}
