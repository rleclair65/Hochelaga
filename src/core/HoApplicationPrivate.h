#pragma once

#include "HoApplication.h"
#include "HoHandleBody.h"
#include "HoEvent.h"

#include <SDL.h>
#include <SDL_opengl.h>

namespace Hochelaga {

class ApplicationPrivate
    : public Body<Application>
    , public EventQueue
{
public:
     ApplicationPrivate( Application * handle, const String & name, int argc, char * argv[]);

     const ViewSPtr & getTopView() const  { return topView; }
     void setTopView(const ViewSPtr & newTopView)  { topView = newTopView; }

     Application::State getState() const    { return state; }
     const Point2d & getScreenSize() const  { return screenSize; }
     Scalar getScreenDpi() const            { return screenDpi; }

     const String findAssetFilePath( const String & fileName ) const;

     int run();
     void terminate( int resultCode = 0 );
     void paint();


protected:
    bool initSdl2();
    void closeSdl2();
    void readSdl2Event( int timeout );

    void onEvent( const EventSPtr & event );

    void convertSdlKeyDownEvent( const SDL_KeyboardEvent & keyEvent );
    void convertSdlTexInputEvent( const SDL_TextInputEvent & textEvent );
    void convertSdlMouseButtonDown( const SDL_MouseButtonEvent & buttonEvent );
    void convertSdlMouseButtonUp( const SDL_MouseButtonEvent & buttonEvent );
    void convertSdlMouseMotion( const SDL_MouseMotionEvent & motionEvent );


private:
    String name;
    Application::ArguementList arguementList;
    ViewSPtr topView;
    Application::State state;
    Point2d screenSize;
    Scalar screenDpi;
    int resultCode;
    SDL_Window * mainWindow;
    SDL_GLContext mainGLContext;
    PaintContext mainPaintContext;
    bool viewAnimated;
    static Application * instance;
};

}
