/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include <stdint.h>
#include <time.h>
#include <boost/shared_ptr.hpp>

namespace Hochelaga {

class Event;
typedef boost::shared_ptr<Event> EventSPtr;

class Event
{
public:
    enum Type
    {
        appStateChangedEventType,
        keyboardEventType,
        touchPressedEventType,
        touchUnpressedEventType,
        touchEnteredEventType,
        touchLeavedEventType,
        touchMovedEventType,
        touchCancelledEventType,

        customEventType = 1000
    };

public:
    Event( Type type, uint32_t timestamp );
    virtual ~Event();

    Type getType() const  { return type; }
    uint32_t getTimestamp() const  { return timestamp; }


private:
    Type type;
    uint32_t timestamp;
};

}
