/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoTypes.h"

namespace Hochelaga {

class Dimension
{
public:
    enum Unit {
        unknownUnit, // dont know value and unit

        pxUnit, // physical pixel
        inUnit, // Inches - based on the physical size of the screen. 1 Inch = 2.54 centimeters
        mmUnit, // Millimeters - based on the physical size of the screen.
        ptUnit, // Points - 1/72 of an inch based on the physical size of the screen.

        dipUnit, // Density-independent Pixels - an abstract unit that is based on the physical density of the
                 // screen. These units are relative to a 160 dpi screen, so one dip is one pixel on a 160 dpi
                 // screen. The ratio of dp-to-pixel will change with the screen density, but not necessarily in
                 // direct proportion.

        screenWidthRelativeUnit,
        screenHeightRelativeUnit,

        parentWidthRelativeUnit,
        parentHeightRelativeUnit,

        remainingUnit
    };


    Scalar getValue() const  { return value; }
    Unit getUnit() const     { return unit; }

public:
    Dimension( Scalar value = 0.0f, Unit unit = unknownUnit );

private:
    Scalar value;
    Unit unit;
};

}

