/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include <string>

namespace Hochelaga {

class ByteArray;

class String
    : public std::wstring
{
public:
    String();

    size_t getByteLength() const  { return size() * sizeof( wchar_t); }

    static String fromUtf8( const char * utf8Data, size_t utf8Size );
    static String fromUtf8( const char * utf8Str );
    static String fromUtf8( const ByteArray & byteArray );
    const ByteArray toUtf8() const;

    static String fromUcs16( const wchar_t * ucs16Str );
    const wchar_t * toUcs16() const;
};

}

