/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoTypes.h"
#include "HoPoint.h"

namespace Hochelaga {

class Rect2d
{
public:
    Rect2d( Scalar left = 0.0f, Scalar top = 0.0f, Scalar right = 0.0f, Scalar bottom = 0.0f )
        : left( left )
        , top( top )
        , right( right )
        , bottom( bottom )
    {
    }
    Rect2d( const Point2d & pt1, const Point2d & pt2 );
    Rect2d( const Point2d & topLeft, Scalar width, Scalar height );

    Scalar getLeft() const    { return left; }
    Scalar getTop() const     { return top; }
    Scalar getRight() const   { return right; }
    Scalar getBottom() const  { return bottom; }

    const Point2d getLeftTop() const;


private:
    Scalar left;
    Scalar top;
    Scalar right;
    Scalar bottom;
};

}
