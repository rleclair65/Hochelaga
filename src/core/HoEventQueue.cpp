/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoEventQueue.h"

namespace Hochelaga {

EventQueue::EventQueue()
{
}

void EventQueue::pushEvent(const EventSPtr & event)
{
    eventList.push( event );
}

void EventQueue::processAllEvents()
{
    while( ! eventList.empty() )
    {
        EventSPtr event = eventList.front();
        onEvent(event);
        eventList.pop();
    }
}

}
