#pragma once

#include "HoView.h"

namespace Hochelaga
{

class ViewPrivate
        : public Body<View>
{
public:
    ViewPrivate( View * handle, const View::MeasureInfo & measureInfo, View::Alignment alignment );

    Scalar getTransparency() const                 { return tranparency; }
    void setTransparency( Scalar newTranparency )  { tranparency = newTranparency; }

    const Point2d & getOrigin() const  { return origin; }
    void setOrigin(const Point2d & newOrigin);

    Scalar getZ() const                 { return z; }
    void setZ( Scalar newZ )            { z = newZ; }

    const Point2d & getSize() const  { return size; }
    void setSize( const Point2d & newSize);

    View::Alignment getAlignment() const  { return alignment; }

    const Rect2d & getMargins() const              { return margins; }
    void setMargins( const Rect2d & newMargins )   { margins = newMargins; }

    const Rect2d & getPadding() const              { return padding; }
    void setPadding( const Rect2d & newPadding )   { padding = newPadding; }

    const Color & getBackgroundColor() const           { return backgroundColor; }
    void setBackgroundColor( const Color & newColor )  { backgroundColor = newColor; }

    void addChildView( const ViewSPtr & childView );
    void removeChildView( const ViewSPtr & childView );
    size_t getChildViewCount() const;
    ViewSPtr getChildView(size_t ndx) const;

    ViewSPtr getParentView();


    void requestPaint();
    virtual bool updatePaint( TimeInMicroSeconds currentTime );
    virtual void paint( const PaintContext & context );

    void requestLayout();
    bool isLayoutRequested() const  { return layoutRequested; }
    virtual void layout(const Point2d & newSize);
    virtual const Point2d calculatePreferredSize();

    void requestFocus();
    static ViewSPtr getFocusedView()  { return focusedView.lock(); }

    virtual void onEvent( const EventSPtr & event );

    virtual const Point2d mapFromParent(const Point2d & parentPoint);
    virtual const Point2d mapToParent(const Point2d & point);
    const Point2d mapFromScreen(const Point2d & screenPoint);
    const Point2d mapToScreen(const Point2d & point);


protected:
    typedef std::vector< ViewSPtr > ChildViewVector;

    Scalar tranparency;
    Point2d origin;

    /** \brief z position relative to parent view to create shadow effets */
    Scalar z;

    Point2d size;

    View::Alignment alignment;

    Rect2d margins;
    Rect2d padding;
    View::MeasureInfo measureInfo;
    Color backgroundColor;

    ViewWPtr parentView;
    ChildViewVector childViewVector;

    static ViewWPtr focusedView;
    bool layoutRequested;
    bool paintRequested;


protected:
    virtual void onAppStateChangedEvent( const ApplicationStateChangedEvent & appStateChangedEvent );
    virtual void onKeyboardEvent( const KeyboardEvent & keyboardEvent );
    virtual bool onTouchEvent( const TouchEvent & touchEvent );

    void paintChildren( const PaintContext & context );
    void paintBackground( const PaintContext & context );
    Scalar toPixel( const Dimension & dimension ) const;
    const Point2d clampSize( const Point2d & size ) const;
    void setParentView( const ViewSPtr & newParentView );
};

}
