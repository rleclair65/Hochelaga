/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoString.h"
#include "HoByteArray.h"

namespace Hochelaga {

String::String()
{
}

String String::fromUtf8( const char * utf8Data, size_t utf8Size )
{
    String result;

    for ( size_t i = 0; i < utf8Size; i++ )
        result += (wchar_t)utf8Data[i];

    return result;
}

String String::fromUtf8( const char * utf8Str )
{
    return fromUtf8( utf8Str, strlen(utf8Str) );
}

String String::fromUtf8( const ByteArray & byteArray )
{
    return fromUtf8( byteArray, byteArray.size() );
}

const ByteArray String::toUtf8() const
{
    ByteArray result;

    for( const_iterator iter = begin(); iter != end(); iter++ )
    {
        wchar_t curChar = *iter;
        if ( curChar < 128 )
            result.push_back( (uint8_t)curChar );
    }

    // terminating null
    result.push_back( 0 );

    return result;
}

}
