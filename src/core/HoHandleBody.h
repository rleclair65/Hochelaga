#pragma once

#include <boost/shared_ptr.hpp>

namespace Hochelaga {

template <typename B>
class Handle
{
public:
    Handle( const boost::shared_ptr<B> & body )
        : body( body )
    {
    }

public:
    const boost::shared_ptr<B> & getBody() const  { return body; }

protected:
    template<typename P>
    boost::shared_ptr<P> dynamicBodyCast() const
    {
        return boost::dynamic_pointer_cast<P>(body);
    }

private:
    boost::shared_ptr<B> body;
};


template <typename H>
class Body
{
public:
    Body( H * handle )
        : handle( handle )
    {
    }

protected:
   H * getHandle() const  { return handle; }


private:
    H * handle;
};

}

