/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoLog.h"
#include <SDL.h>

namespace Hochelaga {

void Log::verbose(const char * fmt, ...)
{
    va_list vl;
    va_start(vl,fmt);

    char message[500];
    vsprintf(message,fmt,vl);
    SDL_LogVerbose(SDL_LOG_CATEGORY_APPLICATION, "VERBOSE: %s", message);

    va_end(vl);
}

void Log::info(const char * fmt, ...)
{
    va_list vl;
    va_start(vl,fmt);

    char message[500];
    vsprintf(message,fmt,vl);
    SDL_LogInfo(SDL_LOG_CATEGORY_APPLICATION, "INFO: %s", message);

    va_end(vl);
}

void Log::warn(const char * fmt, ...)
{
    va_list vl;
    va_start(vl,fmt);

    char message[500];
    vsprintf(message,fmt,vl);
    SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "WARNING: %s", message);

    va_end(vl);
}

void Log::error(const char * fmt, ...)
{
    va_list vl;
    va_start(vl,fmt);

    char message[500];
    vsprintf(message,fmt,vl);
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "ERROR: %s", message);

    va_end(vl);
}

}
