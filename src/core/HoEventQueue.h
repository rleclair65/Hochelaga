/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoEvent.h"

#include <queue>

namespace Hochelaga {

class EventQueue
{
public:
    EventQueue();

    void pushEvent(const EventSPtr & event);
    void processAllEvents();


protected:
    virtual void onEvent( const EventSPtr & event ) = 0;


private:
    typedef std::queue<EventSPtr> EventList;

    EventList eventList;
};

}
