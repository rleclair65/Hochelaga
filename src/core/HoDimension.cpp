/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoDimension.h"

namespace Hochelaga {

Dimension::Dimension( Scalar value, Unit unit )
    : value( value )
    , unit( unit )
{
}

}
