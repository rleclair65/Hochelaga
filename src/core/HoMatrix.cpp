/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoMatrix.h"

namespace Hochelaga {

Matrix3d::Matrix3d()
{
    for ( int i = 0; i < 16; i++ ) {
        if ( (i % 5) == 0 )
            data[ i ] = 1.0f;
        else
            data[ i ] = 0.0f;
    }
}

bool Matrix3d::operator == ( const Matrix3d & otherMatrix ) const
{
    for ( int i = 0; i < 16; i++ )
    {
        if ( otherMatrix.data[ i ] != data[ i ] )
            return false;
    }
    return true;
}

}

