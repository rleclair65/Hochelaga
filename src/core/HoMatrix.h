/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "HoTypes.h"

namespace Hochelaga {

class Matrix3d
{
public:
    Matrix3d();

    bool operator == ( const Matrix3d & otherMatrix ) const;

private:
    Scalar data[ 16 ];
};

}
