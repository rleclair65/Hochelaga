#pragma once

namespace Hochelaga {

class Rational
{
public:
    Rational(int numerator = 0, int denumerator = 0);

    int getNumerator() const    { return numerator; }
    int getDenumerator() const  { return denumerator; }

    bool isValid() const  { return (denumerator != 0); }

private:
    int numerator;
    int denumerator;
};

}
