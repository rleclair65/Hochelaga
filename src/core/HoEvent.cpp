/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoEvent.h"

namespace Hochelaga {

Event::Event(Type type, uint32_t timestamp )
    : type( type )
    , timestamp( timestamp )
{
}

Event::~Event()
{
}

}
