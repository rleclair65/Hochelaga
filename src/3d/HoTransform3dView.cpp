/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoTransform3dView.h"

namespace Hochelaga {

Transform3dView::Transform3dView( const MeasureInfo & measureInfo, Alignment alignment )
    : View( measureInfo, alignment )
    , gestureEnabled( false )
{
}

void Transform3dView::setTransformMatrix( const Matrix3d & newMatrix )
{
    if ( newMatrix == transformMatrix )
        return;

    transformMatrix = newMatrix;
    requestPaint();
}

void Transform3dView::enableGesture( bool enabled )
{
    if ( gestureEnabled == enabled )
        return;

    gestureEnabled = enabled;
}

}
