/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "../core/HoView.h"

#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread/future.hpp>

namespace Hochelaga {

class Scene3dView
    : public View
{
public:
    Scene3dView( const MeasureInfo & measureInfo,
                 Alignment alignment );

    boost::future<int> loadScene( const String & sceneUrl );
    void unload();
};

}
