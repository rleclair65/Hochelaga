/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "../core/HoView.h"
#include "../core/HoMatrix.h"

namespace Hochelaga {

class Transform3dView
    : public View
{
public:
    Transform3dView( const MeasureInfo & measureInfo = MeasureInfo(), Alignment alignment = noAlignment );

    void setTransformMatrix( const Matrix3d & newMatrix );
    const Matrix3d & getTransformMatrix() const  { return transformMatrix; }

    void enableGesture( bool enabled );
    bool isGestureEnabled() const  { return gestureEnabled; }

private:
    Matrix3d transformMatrix;
    bool gestureEnabled;
};

}
