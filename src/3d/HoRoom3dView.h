/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */
#pragma once

#include "../core/HoView.h"

namespace Hochelaga {

class Room3dView
    : public View
{
public:
    Room3dView( const MeasureInfo & measureInfo = MeasureInfo(), Alignment alignment = noAlignment );
};

}

