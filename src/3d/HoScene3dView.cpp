/*
 * Copyright 2015 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */
#include "HoScene3dView.h"

namespace Hochelaga {

Scene3dView::Scene3dView( const MeasureInfo & measureInfo,
                          Alignment alignment )
    : View( measureInfo, alignment )
{
}

}
