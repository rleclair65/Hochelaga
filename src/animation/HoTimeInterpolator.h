/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

namespace Hochelaga {

class TimeInterpolator
{
public:
    virtual ~TimeInterpolator();

    virtual float getInterpolation( float ) = 0;
};

class AccelerateDecelerateInterpolator
    : public TimeInterpolator
{
public:
    AccelerateDecelerateInterpolator();

    float getInterpolation( float );
};

class AccelerateInterpolator
    : public TimeInterpolator
{
public:
    AccelerateInterpolator();

    float getInterpolation( float input );
};

class AnticipateInterpolator
    : public TimeInterpolator
{
public:
    AnticipateInterpolator();

    float getInterpolation( float input );
};

class AnticipateOvershootInterpolator
    : public TimeInterpolator
{
public:
    AnticipateOvershootInterpolator();

    float getInterpolation( float input );
};

class BounceInterpolator
    : public TimeInterpolator
{
public:
    BounceInterpolator();

    float getInterpolation( float input );
};

class CycleInterpolator
    : public TimeInterpolator
{
public:
    CycleInterpolator();

    float getInterpolation( float input );
};

class DecelerateInterpolator
    : public TimeInterpolator
{
public:
    DecelerateInterpolator();

    float getInterpolation( float input );
};

class FastOutLinearInterpolator
    : public TimeInterpolator
{
public:
    FastOutLinearInterpolator();

    float getInterpolation( float input );
};

class FastOutSlowInterpolator
    : public TimeInterpolator
{
public:
    FastOutSlowInterpolator();

    float getInterpolation( float input );
};

class LinearInterpolator
    : public TimeInterpolator
{
public:
    LinearInterpolator();

    float getInterpolation( float input );
};

class LinearOutSlowInterpolator
    : public TimeInterpolator
{
public:
    LinearOutSlowInterpolator();

    float getInterpolation( float input );
};

class OvershootInterpolator
    : public TimeInterpolator
{
public:
    OvershootInterpolator();

    float getInterpolation( float input );
};


}
