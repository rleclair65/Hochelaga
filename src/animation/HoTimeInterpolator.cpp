/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoTimeInterpolator.h"

namespace Hochelaga {

TimeInterpolator::~TimeInterpolator()
{
}

//=================================================================================================================================
AccelerateDecelerateInterpolator::AccelerateDecelerateInterpolator()
{
}

float AccelerateDecelerateInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
AccelerateInterpolator::AccelerateInterpolator()
{
}

float AccelerateInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
AnticipateInterpolator::AnticipateInterpolator()
{
}

float AnticipateInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
AnticipateOvershootInterpolator::AnticipateOvershootInterpolator()
{
}

float AnticipateOvershootInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
BounceInterpolator::BounceInterpolator()
{
}

float BounceInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
CycleInterpolator::CycleInterpolator()
{
}

float CycleInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
DecelerateInterpolator::DecelerateInterpolator()
{
}

float DecelerateInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
FastOutLinearInterpolator::FastOutLinearInterpolator()
{
}

float FastOutLinearInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
FastOutSlowInterpolator::FastOutSlowInterpolator()
{
}

float FastOutSlowInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
LinearInterpolator::LinearInterpolator()
{
}

float LinearInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
LinearOutSlowInterpolator::LinearOutSlowInterpolator()
{
}

float LinearOutSlowInterpolator::getInterpolation( float input )
{
    return input;
}

//=================================================================================================================================
OvershootInterpolator::OvershootInterpolator()
{
}

float OvershootInterpolator::getInterpolation( float input )
{
    return input;
}

}
