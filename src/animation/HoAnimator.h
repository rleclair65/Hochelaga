/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */
#pragma once

#include "../core/HoTime.h"
#include "../core/HoPoint.h"
#include "../core/HoColor.h"

namespace Hochelaga {

class Animator
{
public:
    Animator();

    void setDuration( TimeInMicroSeconds duration );

    void startAt( TimeInMicroSeconds time );
    void pauseAt( TimeInMicroSeconds time );
    void resumeAt( TimeInMicroSeconds time );
    void stopAt( TimeInMicroSeconds time );

    bool isRunning();
};

class IntAnimator
    : public Animator
{
public:
    IntAnimator( int fromValue, int toValue );

    int getValueAt( TimeInMicroSeconds time );
};

class FloatAnimator
    : public Animator
{
public:
    FloatAnimator( float fromValue, float toValue );

    float getValueAt( long long time );
};

class DoubleAnimator
    : public Animator
{
public:
    DoubleAnimator( double fromValue, double toValue );

    float getValueAt( TimeInMicroSeconds time );
};

class Point2dAnimator
    : public Animator
{
public:
    Point2dAnimator( const Point2d & fromValue, const Point2d & toValue );

    const Point2d getValueAt( TimeInMicroSeconds time );
};

class Point3dAnimator
    : public Animator
{
public:
    Point3dAnimator( const Point3d & fromValue, const Point3d & toValue );

    const Point3d getValueAt( TimeInMicroSeconds time );
};

class ColorAnimator
    : public Animator
{
public:
    ColorAnimator( const Color & fromValue, const Color & toValue );

    const Color getValueAt( TimeInMicroSeconds time );
};

}

