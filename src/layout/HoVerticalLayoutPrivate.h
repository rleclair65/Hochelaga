#pragma once

#include "HoVerticalLayout.h"
#include "../core/HoViewPrivate.h"

namespace Hochelaga
{

class VerticalLayoutPrivate
    : public ViewPrivate
{
public:
    VerticalLayoutPrivate( VerticalLayout * handle, const View::MeasureInfo & measureInfo, View::Alignment alignment );


    //
    // ViewPrivate class overriden
    //
public:
    void layout(const Point2d & newSize);

};

}
