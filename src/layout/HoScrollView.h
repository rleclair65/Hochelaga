/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "core/HoView.h"

namespace Hochelaga {

class ScrollView
    : public View
{
    enum Style
    {
        noHorizontalScrollStyle                 = 0x00,
        invisibleHorizontalScrollbarStyle       = 0x01,
        alwaysVisibleHorizontalScrollbarStyle   = 0x02,
        visibleOnScrollHorizontalScrollbarStyle = 0x03,
        horizontalMaskStyle                     = 0x0f,

        noVerticalScrollbarStyle                = 0x00,
        invisibleVerticalScrollbarStyle         = 0x10,
        alwaysVisibleVerticalScrollbarStyle     = 0x20,
        visibleOnScrollVerticalScrollbarStyle   = 0x30,
        verticalMaskStyle                       = 0xf0
    };


public:
    ScrollView( const ViewSPtr & contentView, Style style,
                const MeasureInfo & measureInfo = MeasureInfo(), Alignment alignment = noAlignment );

    const ViewSPtr & getContentView() const  { return contentView; }
    Style getStyle() const                   { return style; }


    //
    // View class overiden
    //
public:
    const Point2d calculatePreferredSize();
    void paint( const PaintContext & context );


private:
    ViewSPtr contentView;
    Style style;
    Point2d contentOffset;
};

}

