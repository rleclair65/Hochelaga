/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoVerticalLayoutPrivate.h"

#include <boost/make_shared.hpp>


namespace Hochelaga {

VerticalLayout::VerticalLayout( const MeasureInfo & measureInfo, Alignment alignment )
    : View(boost::make_shared<VerticalLayoutPrivate>(this, measureInfo, alignment))
{
}


//=================================================================================================================================
VerticalLayoutPrivate::VerticalLayoutPrivate( VerticalLayout * handle,
                                              const View::MeasureInfo & measureInfo,
                                              View::Alignment alignment )
    : ViewPrivate(handle, measureInfo, alignment)
{
}

void VerticalLayoutPrivate::layout(const Point2d & newSize)
{
    size = newSize;
    layoutRequested = false;
    paintRequested = true;

    Point2d curOrigin(0.0f, 0.0f);

    for ( auto childView : childViewVector )
    {
        Point2d childPreferredSize = childView->calculatePreferredSize();
        View::Alignment childAlignment = childView->getAlignment();

        switch( childAlignment & View::horizontalAlignmentMask )
        {
        case View::leftAlignment:
        case View::noAlignment:
            curOrigin.setX(0.0f);
            break;

        case View::centerAlignment:
            curOrigin.setX((newSize.getX() - childPreferredSize.getX()) / 2);
            break;

        case View::rightAlignment:
            curOrigin.setX(newSize.getX() - childPreferredSize.getX());
            break;
        }

        childView->setOrigin(curOrigin);
        childView->layout(childPreferredSize);

        curOrigin.setY( curOrigin.getY() + childPreferredSize.getY());
    }
}

}
