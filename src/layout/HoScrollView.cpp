/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoScrollView.h"
#include <assert.h>

namespace Hochelaga {

ScrollView::ScrollView( const ViewSPtr & contentView,
                        Style style,
                        const MeasureInfo & measureInfo,
                        Alignment alignment
                        )
    : View( measureInfo, alignment )
    , contentView( contentView )
    , style( style )
{
    assert( contentView );
    addChildView( contentView );
}

#if TODO
const Point2d ScrollView::calculatePreferredSize()
{
    Point2d result;
    if ( (measureInfo.preferredHeight.getUnit() == Dimension::unknownUnit) ||
         (measureInfo.preferredWidth.getUnit() == Dimension::unknownUnit)
        ) {
        result = contentView->calculatePreferredSize();

        if ( measureInfo.preferredWidth.getUnit() == Dimension::unknownUnit)
             result.setX( toPixel( measureInfo.preferredWidth ) );

        if ( measureInfo.preferredHeight.getUnit() == Dimension::unknownUnit)
             result.setY( toPixel( measureInfo.preferredHeight ) );
    } else
        result = Point2d( toPixel( measureInfo.preferredWidth ), toPixel( measureInfo.preferredHeight ) );


    //
    // clamps to min / max
    //
    return clampSize( result );
}

void ScrollView::paint( const PaintContext & context )
{
}
#endif

}
