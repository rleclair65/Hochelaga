/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#pragma once

#include "core/HoView.h"

namespace Hochelaga {

class GridLayout
        : public View
{
public:
    GridLayout( const MeasureInfo & measureInfo = MeasureInfo(), Alignment alignment = noAlignment );

    void addColumn( const Dimension & dimension );
    void addRow( const Dimension & dimension );
    void addChildView( const ViewSPtr & childView, int rowNum, int colNum, int rowSpan, int colSpan );


    //
    // View class overriden
    //

public:
    void layout( Scalar newWidth, Scalar newHeight );
};

}
