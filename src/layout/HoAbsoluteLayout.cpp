/*
 * Copyright 2016 Robert Leclair.
 *
 * Use of this source code is governed by LGPL v3 license that can be
 * found in the LICENSE file.
 */

#include "HoAbsoluteLayout.h"

namespace Hochelaga {

AbsoluteLayout::AbsoluteLayout(const MeasureInfo &measureInfo, Alignment alignment)
    : View( measureInfo, alignment )
{

}

#ifdef TODO
void AbsoluteLayout::layout( Scalar newWidth, Scalar newHeight )
{
    width = newWidth;
    height = newHeight;
    layoutRequested = false;
    paintRequested = true;

    for ( auto childView : childViewVector )
    {
        Point2d childPreferredSize = childView->calculatePreferredSize();
        childView->layout( childPreferredSize.getX(), childPreferredSize.getY() );
    }
}
#endif

}
